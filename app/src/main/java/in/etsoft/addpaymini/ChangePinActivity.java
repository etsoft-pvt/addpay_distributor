package in.etsoft.addpaymini;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import in.etsoft.addpaymini.model.MyResponse;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.SessionManager;
import in.etsoft.addpaymini.utility.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePinActivity extends AppCompatActivity {
    private static final String TAG = ChangePinActivity.class.getSimpleName();
    Context context = ChangePinActivity.this;
    private EditText etCurrentPin, etNewPin,etNewPinAgain;
    private Button btnSubmitChangePin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        setTitle("Pin Management");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadView();
    }

    private void loadView() {
        etCurrentPin = (EditText) findViewById(R.id.etCurrentPin);
        etNewPin = (EditText) findViewById(R.id.etNewPin);
        etNewPinAgain = (EditText) findViewById(R.id.etNewPinAgain);
        btnSubmitChangePin = (Button) findViewById(R.id.btnSubmitChangePin);
        btnSubmitChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActionForAll.validPassword(etCurrentPin, "pin", ChangePinActivity.this) &&
                        ActionForAll.validPassword(etNewPin, "pin", ChangePinActivity.this) &&
                        ActionForAll.validPassword(etNewPinAgain, "pin", ChangePinActivity.this)) {
                    if(etNewPin.getText().toString().equals(etNewPinAgain.getText().toString()))
                    {
                        new AlertDialog.Builder(context)
                                .setTitle(R.string.app_name)
                                .setMessage("Do you want to change password!")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        changePassword();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                    }
                    else{
                        etNewPinAgain.setText("");
                        etNewPinAgain.setError("Pin mismatch please re-enter pin!");
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changePassword(){

        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MyResponse> call = apiService.changePassword(SessionManager.getInstance(context).getString(SessionManager.USER_LOGIN_ID),
                etCurrentPin.getText().toString(),
                etNewPin.getText().toString());
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {

                Util.hideProgressDialog(progressDialog);
                if(response!=null && response.isSuccessful()){
                    MyResponse login = response.body();
                    try{
                        if(login.getCode() == 200){

                            new AlertDialog.Builder(context)
                                    .setTitle(R.string.app_name)
                                    .setMessage(login.getMessage())
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            Intent intent = new Intent(context, LoginActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    }).create().show();
                            //Log.e(TAG, " code : " + login.getCode() +" message : " + login.getMessage() + "  date : " + login.getUserDetails().getAiDate() );
                        }
                        else{
                            etCurrentPin.setText("");
                            etNewPin.setText("");
                            etNewPinAgain.setText("");
                            etCurrentPin.requestFocus();
                            ActionForAll.alertUser(login.getMessage(),"Ok", ChangePinActivity.this);
                            Log.e(TAG, "message : " + login.getMessage() + "  code : " + login.getCode() );
                        }

                    }catch (Exception e){
                        Log.e(TAG, "exception  " + e.toString());
                    }
                }
                Log.e(TAG, "onResponse : " + response.toString());
            }
            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }
}