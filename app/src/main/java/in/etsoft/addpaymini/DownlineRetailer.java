package in.etsoft.addpaymini;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownlineRetailer {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("down_retailer_count")
    @Expose
    private Integer retailerCount;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getRetailerCount() {
        return retailerCount;
    }

    public void setRetailerCount(Integer retailerCount) {
        this.retailerCount = retailerCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
