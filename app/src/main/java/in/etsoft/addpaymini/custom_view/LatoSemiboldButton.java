package in.etsoft.addpaymini.custom_view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

public class LatoSemiboldButton extends AppCompatButton {

    public LatoSemiboldButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoSemiboldButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoSemiboldButton(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Semibold.ttf");

        setTypeface(tf);
    }
}
