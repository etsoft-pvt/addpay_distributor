package in.etsoft.addpaymini.custom_view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class LatoSemiboldTextView extends AppCompatTextView {

    public LatoSemiboldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoSemiboldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoSemiboldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Lato-Semibold.ttf");

        setTypeface(tf);
    }
}
