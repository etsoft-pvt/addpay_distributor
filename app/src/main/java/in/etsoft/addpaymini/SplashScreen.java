package in.etsoft.addpaymini;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import in.etsoft.addpaymini.utility.VersionChecker;

public class SplashScreen extends AppCompatActivity {

    private static final String TAG = SplashScreen.class.getSimpleName();
    public static String PACKAGE_NAME;
    public static final int WAIT_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        PACKAGE_NAME = getApplicationContext().getPackageName();
        Log.e(TAG, "onCreate: " + PACKAGE_NAME );

        try {
            VersionChecker versionChecker = new VersionChecker();
            String versionUpdated = versionChecker.execute().get();
            //Log.i(TAG,"version code is " + versionUpdated);

            PackageInfo packageInfo = null;
            try {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            int version_code = packageInfo.versionCode;
            String version_name = packageInfo.versionName;
            //versionUpdated = "1.1";
            //Log.i(TAG, String.valueOf(version_code) + "  " + version_name);
            double dbl_version_name =  Double.parseDouble(version_name);
            double dbl_versionUpdated =  Double.parseDouble(versionUpdated);
            Log.i(TAG, "play store version : " + versionUpdated);
            Log.i(TAG, "my_app_version: " + version_name);
            Log.i(TAG, "double_my_app_version: " + dbl_version_name);
            Log.i(TAG, "double_play store version: " + dbl_versionUpdated);
            if (dbl_versionUpdated > dbl_version_name) {
            //if (versionUpdated.equalsIgnoreCase(version_name)) {
                String packageName = getApplicationContext().getPackageName();//
                /*UpdateMeeDialog updateMeeDialog = new UpdateMeeDialog();
                updateMeeDialog.showDialogAddRoute(MainActivity.this, packageName);*/
                //Toast.makeText(getApplicationContext(), "please updated " + versionUpdated + " name : " + version_name, Toast.LENGTH_LONG).show();
                new AlertDialog.Builder(SplashScreen.this)
                        .setTitle(R.string.app_name)
                        .setMessage("New update available on google play store.You need to update to the app")
                        .setCancelable(false)
                        .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+ packageName));
                                startActivity(intent);
                                finish();
                            }
                        }).create().show();
            }
            else{
                Log.e(TAG, "both version same or update version lower to device version");
                new Handler().postDelayed(() -> {
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    finish();
                },WAIT_TIME);
            }
        } catch (Exception e) {
            e.getStackTrace();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                    finish();
                }
            },WAIT_TIME);
        }
    }
}