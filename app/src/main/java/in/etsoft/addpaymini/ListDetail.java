package in.etsoft.addpaymini;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import in.etsoft.addpaymini.adapters.FormAdapter;
import in.etsoft.addpaymini.model.formlist.Forms;
import in.etsoft.addpaymini.model.formlist.LeaveDatum;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.SessionManager;
import in.etsoft.addpaymini.utility.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListDetail extends AppCompatActivity {

    public static final String TAG = ListDetail.class.getSimpleName();
    Context context = ListDetail.this;
    private String[] user_type_master = {"Please Select", "Distributor", "Retailer", "Downline Retailer"};
    private String[] user_type_distributor = {"Please Select", "Retailer"};
    //private String[] user_type_super = {"Master Distributor", "Distributor", "Retailer"};
    Spinner spinUserTypeList;
    ArrayAdapter<String> adapterUserType;
    String user_type = "";
    String user_type_name = "";
    List<LeaveDatum> data;
    LinearLayoutManager linearLayoutManager;
    RecyclerView recFormList;
    FormAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_detail);
        setTitle("Form List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadView();
    }

    private void loadView() {
        recFormList = (RecyclerView) findViewById(R.id.recFormList);
        spinUserTypeList = (Spinner) findViewById(R.id.SpinUserTypeList);
        Log.e(TAG, "user_type " + SessionManager.getInstance(context).getString(SessionManager.USER_TYPE));
        if (SessionManager.getInstance(context).getString(SessionManager.USER_TYPE).equalsIgnoreCase("1")) {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_master);
        } else if (SessionManager.getInstance(context).getString(SessionManager.USER_TYPE).equalsIgnoreCase("2")) {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_distributor);
        } else {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_master);
        }
        adapterUserType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinUserTypeList.setAdapter(adapterUserType);
        spinUserTypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (parent.getItemAtPosition(position).toString()) {
                    case "Downline Retailer":
                        user_type = "3";
                        user_type_name = "Downline Retailer";
                        break;
                    case "Please Select":
                        user_type = "4";
                        user_type_name = "Please Select";
                        break;
                    case "Master Distributor":
                        user_type = "1";
                        user_type_name = "Master Distributor";
                        break;
                    case "Distributor":
                        user_type = "2";
                        user_type_name = "Distributor";
                        break;
                    case "Retailer":
                        user_type = "3";
                        user_type_name = "Retailer";
                        break;
                }

                if (data != null)
                    data.clear();

                if(user_type.equalsIgnoreCase("3") && user_type_name.equalsIgnoreCase("Downline Retailer")){
                    getAllList("1");

                }
                else {
                    getAllList(user_type);
                }

                if (user_type.equalsIgnoreCase("4")) {
                    getAllList("1");
                    recFormList.setVisibility(View.GONE);
                }

                if(data!=null)
                    Log.e(TAG, "user type : "+ user_type + "list size : "+ data.size());
                else
                Log.e(TAG, "user type : "+ user_type + " list size : null ");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllList(String user_type) {
        SessionManager manager = SessionManager.getInstance(this);
        //Log.e(TAG, "reason_for_leave: " + "\nDate From : " + date_from + " \nDate to : " + date_to + "\nReason : " + etLeaveReason.getText().toString());
        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.e(TAG, "user id " + manager.getString(SessionManager.USER_ID) + "/user_type " + user_type);
        Call<Forms> call = apiService.formList(manager.getString(SessionManager.USER_ID), user_type);
        call.enqueue(new Callback<Forms>() {
            @Override
            public void onResponse(Call<Forms> call, Response<Forms> response) {
                Util.hideProgressDialog(progressDialog);
                if (response != null && response.isSuccessful()) {
                    Forms myResponse = response.body();
                    try {
                        if (myResponse.getCode() == 200) {
                            Log.e(TAG, " code : " + myResponse.getCode() + " req_id : " + myResponse.getMessage());
                            //ActionForAll.myFlash(getApplicationContext(), myResponse.getMessage());
                            recFormList.setVisibility(View.VISIBLE);
                            data = myResponse.getLeaveData();
                            linearLayoutManager = new LinearLayoutManager(ListDetail.this);
                            linearLayoutManager.setStackFromEnd(false);
                            //linearLayoutManager.setReverseLayout(true);
                            adapter = new FormAdapter(ListDetail.this, data);
                            recFormList.setLayoutManager(linearLayoutManager);
                            recFormList.setAdapter(adapter);

                            if(user_type_name.equalsIgnoreCase("Retailer")){
                                filterRetailer(SessionManager.getInstance(context).getString(SessionManager.USER_ID));
                            }
                            if(user_type_name.equalsIgnoreCase("Downline Retailer")){
                                filterRetailerDownline(SessionManager.getInstance(context).getString(SessionManager.USER_ID));
                            }
                            if(user_type_name.equalsIgnoreCase("Please Select")){
                                recFormList.setVisibility(View.GONE);
                            }

                        } else {
                            //ActionForAll.myFlash(getApplicationContext(), myResponse.getMessage());
                            recFormList.setVisibility(View.GONE);
                            ActionForAll.alertUser(myResponse.getMessage(), "OK", ListDetail.this);
                            Log.e(TAG, "message : " + myResponse.getMessage() + "  code : " + myResponse.getCode());
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "exception  " + e.toString());
                    }
                }
                Log.e(TAG, "onResponse : " + response.toString());
            }

            @Override
            public void onFailure(Call<Forms> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                //Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }

    private void filterRetailer(String text) {
        List<LeaveDatum> filterList = new ArrayList<LeaveDatum>();
        //looping through existing elements
        for (LeaveDatum s : data) {
            //if the existing elements contains the search input
            Log.e(TAG, "distributor Id : " +s.getMuiMasterDistributorId() + " = = "+ text + " mui id : " + s.getMuiId() + "  list_size " + data.size());
            if (s.getMuiCreatedById().equalsIgnoreCase(text)) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterList);
        if (filterList.size() > 0) {
            recFormList.setVisibility(View.VISIBLE);
            //tvOnUserFound.setVisibility(View.GONE);
        } else {
            recFormList.setVisibility(View.GONE);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.app_name)
                    .setMessage("No Retailer found!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    }).create().show();
            //tvOnUserFound.setVisibility(View.VISIBLE);
        }
    }
    private void filterRetailerDownline(String text) {
        List<LeaveDatum> filterList = new ArrayList<LeaveDatum>();
        //looping through existing elements
        for (LeaveDatum s : data) {
            //if the existing elements contains the search input
            Log.e(TAG, "distributor Id : " +s.getMuiMasterDistributorId() + " = = "+ text + " mui id : " + s.getMuiId() + "  list_size " + data.size());
            if (!s.getMuiCreatedById().equalsIgnoreCase(text)) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterList);
        if (filterList.size() > 0) {
            recFormList.setVisibility(View.VISIBLE);
            //tvOnUserFound.setVisibility(View.GONE);
        } else {
            recFormList.setVisibility(View.GONE);
            new AlertDialog.Builder(this)
                    .setTitle(R.string.app_name)
                    .setMessage("No Retailer found!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    }).create().show();
            //tvOnUserFound.setVisibility(View.VISIBLE);
        }
    }
}