package in.etsoft.addpaymini.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.etsoft.addpaymini.R;
import in.etsoft.addpaymini.model.StateList;

public class StateAdapter extends BaseAdapter {
    Context context;
    List<StateList> list;
    LayoutInflater inflter;

    public StateAdapter(Context applicationContext,  List<StateList> list) {
        this.context = applicationContext;
        this.list = list;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.listitems_layout, null);
        //ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.tvDepart);
        names.setText(list.get(i).getName());
        return view;
    }
}
