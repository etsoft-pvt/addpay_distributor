package in.etsoft.addpaymini.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.util.List;

import in.etsoft.addpaymini.MainActivity;
import in.etsoft.addpaymini.R;
import in.etsoft.addpaymini.model.formlist.LeaveDatum;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.Formstate;

public class FormAdapter extends RecyclerView.Adapter<FormAdapter.QueryReplyHolder> {

    private static final String TAG = FormAdapter.class.getSimpleName();
    private List<LeaveDatum> itemList;
    private Context context;
    //private ArrayList<String> ids;

    public FormAdapter(Context context, List<LeaveDatum> itemList) {
        this.itemList = itemList;
        this.context = context;
        //this.ids = ids;
    }

    @Override
    public FormAdapter.QueryReplyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(context).inflate(R.layout.item_form_list, parent, false);
        FormAdapter.QueryReplyHolder rcv = new FormAdapter.QueryReplyHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(FormAdapter.QueryReplyHolder holder, final int position) {

        holder.tvItemFormItemId.setText(itemList.get(position).getMuiUserId());
        holder.tvItemMobileDate.setText("Mobile : " + itemList.get(position).getMuiMobile());
        holder.tvItemName.setText(ActionForAll.capitalize(itemList.get(position).getMuiFirstName()));
        if(itemList.get(position).getMuiStatus().equalsIgnoreCase("1")){
            holder.tvItemUserType.setText("Active");
            holder.tvItemUserType.setTextColor(Color.parseColor("#009900"));
        }
        else if(itemList.get(position).getMuiStatus().equalsIgnoreCase("0"))
        {
            holder.tvItemUserType.setText("Inactive");
            holder.tvItemUserType.setTextColor(Color.parseColor("#990000"));
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class QueryReplyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvItemFormItemId, tvItemMobileDate, tvItemName,tvItemUserType;
        public LinearLayout lvItemContainer;

        public QueryReplyHolder(View itemView) {
            super(itemView);
            //ivExpterItem = itemView.findViewById(R.id.ivExpterItem);
            tvItemFormItemId = (TextView) itemView.findViewById(R.id.tvItemFormItemId);
            tvItemMobileDate = (TextView) itemView.findViewById(R.id.tvItemMobileDate);
            tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
            tvItemUserType = (TextView) itemView.findViewById(R.id.tvItemUserType);
            lvItemContainer = (LinearLayout) itemView.findViewById(R.id.lvItemContainer);
            lvItemContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            switch (view.getId()) {

                case R.id.lvItemContainer:

                    Formstate.getInstance(context).setString(Formstate.COM_NAME, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiCompanyName()));
                    Formstate.getInstance(context).setString(Formstate.COM_TYPE, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiCompanyType()));
                    Formstate.getInstance(context).setString(Formstate.F_NAME, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiFirstName()));
                    Formstate.getInstance(context).setString(Formstate.L_NAME, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiLastName()));
                    Formstate.getInstance(context).setString(Formstate.MOBILE, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiMobile()));
                    Formstate.getInstance(context).setString(Formstate.EMAIL, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiEmail()));
                    Formstate.getInstance(context).setString(Formstate.ADDRESS, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAddress()));
                    Formstate.getInstance(context).setString(Formstate.GST_NUMBER, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiGstNumber()));
                    Formstate.getInstance(context).setString(Formstate.GST_REGISTRATION, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiGstRegistration()));
                    Formstate.getInstance(context).setString(Formstate.PIN_CODE, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiPinCode()));
                    Formstate.getInstance(context).setString(Formstate.USER_TYPE, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiUserType()));

                    //Formstate.getInstance(context).setString(Formstate.USER_TYPE_NAME, user_type_name);
                    Formstate.getInstance(context).setString(Formstate.STATE_ID, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiStateId()));
                    Formstate.getInstance(context).setString(Formstate.DISTRICT_ID, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiCityId()));
                    Formstate.getInstance(context).setInt(Formstate.PERSONAL_INFO_UPDATE_ID, Integer.parseInt(notNullorEmpty(itemList.get(getAdapterPosition()).getMuiId())));
                    //Formstate.getInstance(context).setInt(Formstate.PERSONAL_INFO_UPDATE_ID, saveInfo.getPersonalInfoUpdateId());
                    Formstate.getInstance(context).setString(Formstate.ACCOUNT_HOLDER_NAME, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAccountHolderName()));
                    Formstate.getInstance(context).setString(Formstate.ACCOUNT_BANK_NAME, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiBankName()));
                    Formstate.getInstance(context).setString(Formstate.ACCOUNT_NUMBER, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAccountNumber()));
                    Formstate.getInstance(context).setString(Formstate.IFSC_CODE, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiIfscCode()));
                    //if(data.getMuiPanNumber()!=null && !data.getMuiPanNumber().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.PAN_NUMBER, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiPanNumber()));
                    //if(data.getMuiAadharNumber()!=null && !data.getMuiAadharNumber().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.AADHAR_NUMBER, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAadharNumber()));
                    //if(data.getMuiPanDocs()!=null && !data.getMuiPanDocs().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.UPLOAD_PAN, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiPanDocs()));
                    //if(data.getMuiAadharBackDocs()!=null && !data.getMuiAadharBackDocs().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.UPLOAD_AADHAR_BACK, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAadharBackDocs()));
                    //if(data.getMuiAadharFrontDocs()!=null && !data.getMuiAadharFrontDocs().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.UPLOAD_AADHAR_FRONT, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiAadharFrontDocs()));
                    //if(data.getMuiMef1()!=null && !data.getMuiMef1().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.UPLOAD_MEF1, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiMef1()));
                    //if(data.getMuiMef2()!=null && !data.getMuiMef2().isEmpty())
                    Formstate.getInstance(context).setString(Formstate.UPLOAD_MEF2, notNullorEmpty(itemList.get(getAdapterPosition()).getMuiMef2()));
                    Formstate.getInstance(context).setString(Formstate.PASS_PORT_PIC, notNullorEmpty(itemList.get(getAdapterPosition()).getMui_passport_photo()));
                    Formstate.getInstance(context).setString(Formstate.PASSBOOK_PIC, notNullorEmpty(itemList.get(getAdapterPosition()).getMui_cancelled_check()));
                    Formstate.getInstance(context).setString(Formstate.PAYMENT_RECEIPT_PIC, notNullorEmpty(itemList.get(getAdapterPosition()).getMui_payment_recive_docs()));
                    Log.i(TAG, "onClick: passport " + itemList.get(getAdapterPosition()).getMui_passport_photo());
                    Log.i(TAG, "onClick: check can " + itemList.get(getAdapterPosition()).getMui_cancelled_check());
                    Log.i(TAG, "onClick: payment receive " + itemList.get(getAdapterPosition()).getMui_payment_recive_docs());

                    String user_type_name = "";

                    switch (notNullorEmpty(itemList.get(getAdapterPosition()).getMuiUserType())) {
                        case "1":
                            user_type_name = "Master Distributor";
                            break;
                        case "2":
                            user_type_name = "Distributor";
                            break;
                        case "3":
                            user_type_name = "Retailer";
                            break;
                    }

                    Formstate.getInstance(context).setString(Formstate.USER_TYPE_NAME, user_type_name);

                    context.startActivity(new Intent(context, MainActivity.class));

                    break;
            }
        }
    }

    private String notNullorEmpty(String data) {
        if (data != null && !data.isEmpty())
            return data;
        return "";
    }

    public void filterList(List<LeaveDatum> filterdNames) {
        this.itemList = filterdNames;
        notifyDataSetChanged();
    }

    /*private String getDate(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(dateString);
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
        return sf.format(date);
    }*/

}
