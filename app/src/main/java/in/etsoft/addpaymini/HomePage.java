package in.etsoft.addpaymini;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import in.etsoft.addpaymini.custom_view.LatoSemiboldTextView;
import in.etsoft.addpaymini.model.Distributor;
import in.etsoft.addpaymini.model.MyResponse;
import in.etsoft.addpaymini.model.Retailer;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.CodeMinimization;
import in.etsoft.addpaymini.utility.Formstate;
import in.etsoft.addpaymini.utility.SessionManager;
import in.etsoft.addpaymini.utility.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePage extends AppCompatActivity {

    private static final String TAG = HomePage.class.getSimpleName();
    Context context;
    LatoSemiboldTextView tvTotalRetailer, tvTotalDistributor,tvTotalDownlineRetailer;
    LinearLayout llTotalDistributorContainer,llTotalRetailerContainer,llTotalDownlineContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_nav);
        context = HomePage.this;
        setUpLayoutWithToolbar();
        loadView();

        String user_type = SessionManager.getInstance(context).getString(SessionManager.USER_TYPE);
        Log.e(TAG, "user_type : " + user_type);
        if(user_type.equalsIgnoreCase("1")){
            viewForMaster();
            distributor_count();
            retailer_count();
            downline_retailer_count();
        }
        else if(user_type.equalsIgnoreCase("2")){
            //distributor_count();
            viewForDistributor();
            retailer_count();
        }
        else if(user_type.equalsIgnoreCase("3")){
            viewForRetailer();
        }

    }

    private void loadView() {
        tvTotalRetailer  = (LatoSemiboldTextView) findViewById(R.id.tvTotalRetailer);
        tvTotalDistributor  = (LatoSemiboldTextView) findViewById(R.id.tvTotalDistributor);
        tvTotalDownlineRetailer  = (LatoSemiboldTextView) findViewById(R.id.tvTotalDownlineRetailer);
        llTotalDistributorContainer  = (LinearLayout) findViewById(R.id.llTotalDistributorContainer);
        llTotalRetailerContainer  = (LinearLayout) findViewById(R.id.llTotalRetailerContainer);
        llTotalDownlineContainer  = (LinearLayout) findViewById(R.id.llTotalDownlineContainer);
    }

    private void setUpLayoutWithToolbar() {
        initToolbar();
        ImageView nav = findViewById(R.id.ivDrawer);

        //CustomTextView title = findViewById(R.id.tvToolClick);
        NavigationView mNavigationView = findViewById(R.id.nav_view);
        View navHeader = mNavigationView.getHeaderView(0);
        ImageView img = navHeader.findViewById(R.id.ivNavProfile);
        TextView nav_name = navHeader.findViewById(R.id.tvNavName);
        try {
            String name = SessionManager.getInstance(HomePage.this).getString(SessionManager.USER_NAME);
            nav_name.setText(ActionForAll.capitalize(name));
        }
        catch (Exception e){}
        //TextView nav_email = navHeader.findViewById(R.id.tvNavEmail);
        //Picasso.with(this).load(sessionManager.getString(SessionManager.PROVIDER_IMAGE)).into(img);
        /* nav_name.setText("ANKIT");
        nav_email.setText("A@GMAIL.COM");*/
        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        CodeMinimization.navListener(nav,mDrawerLayout);
        CodeMinimization.navigationItemListener(mNavigationView, this, mDrawerLayout);

        //Menu nav_Menu = mNavigationView.getMenu();

       /* if (sessionManager.getString(SessionManager.MAIN_PROVIDER).equals("1")) {

            nav_Menu.findItem(R.id.nav_service_charge_mech).setVisible(true);
            nav_Menu.findItem(R.id.nav_Service_pre_service).setVisible(true);
            *//*nav_Menu.findItem(R.id.nav_Service).setVisible(false);
            nav_Menu.findItem(R.id.nav_Serviceable_brand).setVisible(false);*//*
        } else {
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_offer).setVisible(false);
            nav_Menu.findItem(R.id.nav_time_n_schedule).setVisible(false);
            nav_Menu.findItem(R.id.nav_payment_method).setVisible(false);
            nav_Menu.findItem(R.id.nav_time_n_schedule).setVisible(false);
            nav_Menu.findItem(R.id.nav_Serviceable_brand).setVisible(false);
            nav_Menu.findItem(R.id.nav_my_introduction).setVisible(false);
        }*/
    }
    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbarCustom);
        setSupportActionBar(toolbar);
    }

    private void distributor_count() {
        String mui_id = SessionManager.getInstance(context).getString(SessionManager.USER_ID);
        final ProgressDialog progressDialog = Util.showProgressDialog(context);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Distributor> call = apiService.total_distributor(mui_id);
        call.enqueue(new Callback<Distributor>() {
            @Override
            public void onResponse(Call<Distributor> call, Response<Distributor> response) {

                if (response != null && response.isSuccessful()) {

                    Util.hideProgressDialog(progressDialog);
                    Distributor response1 = response.body();
                    if (response1.getCode() == 200) {
                        tvTotalDistributor.setText(response1.getDistributorCount().toString());
                    } else {
                        ActionForAll.myFlash(context, response1.getMessage());
                    }

                    Log.e("home page", response1.getDistributorCount()+"");
                }
            }

            @Override
            public void onFailure(Call<Distributor> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                ActionForAll.myFlash(context, t.getMessage());
            }
        });
    }

    private void retailer_count() {
        String mui_id = SessionManager.getInstance(context).getString(SessionManager.USER_ID);
        final ProgressDialog progressDialog = Util.showProgressDialog(context);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Retailer> call = apiService.total_retailer(mui_id);
        call.enqueue(new Callback<Retailer>() {
            @Override
            public void onResponse(Call<Retailer> call, Response<Retailer> response) {

                if (response != null && response.isSuccessful()) {

                    Util.hideProgressDialog(progressDialog);
                    Retailer response1 = response.body();
                    if (response1.getCode() == 200) {
                        tvTotalRetailer.setText(response1.getRetailerCount().toString());
                    } else {
                        ActionForAll.myFlash(context, response1.getMessage());
                    }

                    Log.e("home page", response1.getRetailerCount()+"");
                }
            }

            @Override
            public void onFailure(Call<Retailer> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                ActionForAll.myFlash(context, t.getMessage());
            }
        });
    }

    private void downline_retailer_count() {
        String mui_id = SessionManager.getInstance(context).getString(SessionManager.USER_ID);
        final ProgressDialog progressDialog = Util.showProgressDialog(context);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DownlineRetailer> call = apiService.total_downline_retailer(mui_id);
        call.enqueue(new Callback<DownlineRetailer>() {
            @Override
            public void onResponse(Call<DownlineRetailer> call, Response<DownlineRetailer> response) {

                if (response != null && response.isSuccessful()) {

                    Util.hideProgressDialog(progressDialog);
                    DownlineRetailer response1 = response.body();
                    if (response1.getCode() == 200) {
                        tvTotalDownlineRetailer.setText(response1.getRetailerCount().toString());
                    } else {
                        ActionForAll.myFlash(context, response1.getMessage());
                    }

                    Log.e("home page", response1.getRetailerCount()+"");
                }
            }

            @Override
            public void onFailure(Call<DownlineRetailer> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                ActionForAll.myFlash(context, t.getMessage());
            }
        });
    }

    private void viewForMaster(){
        llTotalDistributorContainer.setVisibility(View.VISIBLE);
        llTotalRetailerContainer.setVisibility(View.VISIBLE);
        llTotalDownlineContainer.setVisibility(View.VISIBLE);

    }

    private void viewForDistributor(){
        llTotalDistributorContainer.setVisibility(View.GONE);
        llTotalRetailerContainer.setVisibility(View.VISIBLE);
        llTotalDownlineContainer.setVisibility(View.GONE);

    }

    private void viewForRetailer(){
        llTotalDistributorContainer.setVisibility(View.GONE);
        llTotalRetailerContainer.setVisibility(View.GONE);
        llTotalDownlineContainer.setVisibility(View.GONE);

    }
}