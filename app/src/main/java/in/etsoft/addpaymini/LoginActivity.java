package in.etsoft.addpaymini;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.chaos.view.PinView;

import in.etsoft.addpaymini.model.login.Login;
import in.etsoft.addpaymini.model.login.UserDetails;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.SessionManager;
import in.etsoft.addpaymini.utility.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static Context context;
    EditText etLoginName, etLoginPasssword;
    Button btnLogin;
    Handler handler;
    PinView pinViewLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        context = getApplicationContext();
        etLoginName = (EditText)findViewById(R.id.etLoginName);
        //etLoginPasssword = (EditText)findViewById(R.id.etLoginPassword);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        pinViewLogin = (PinView)findViewById(R.id.pinViewLogin);;
        etLoginName.requestFocus();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkField(etLoginName) && checkPin(pinViewLogin)){
                    String mobile = etLoginName.getText().toString();
                    String pin = pinViewLogin.getText().toString();

                    getLogin(mobile, pin);
                    /*if(ActionForAll.isNetworkConnected(LoginActivity.this))
                    {
                        getLogin(mobile, pin);
                    }*/
                }
            }
        });
        try{
            if(SessionManager.getInstance(context).getString(SessionManager.USER_MOBILE).length() > 0){
                etLoginName.setText(SessionManager.getInstance(context).getString(SessionManager.USER_MOBILE));
                pinViewLogin.requestFocus();
            }
        }catch (Exception e){}
    }

    private boolean checkField(EditText txt){

        if(txt.getText().toString().trim().length() == 10){
            return true;
        }
        txt.requestFocus();
        txt.setError("Please enter 10 digit mobile number");
        return false;
    }

    private boolean checkPin(PinView p){
        if(p.getText().toString().length()==4){
            return true;
        }

        p.requestFocus();
        p.setError("Please enter 4 digit pin");
        //Toast.makeText(LoginActivity.this,  p.getText().toString().length() +"  ", Toast.LENGTH_SHORT).show();
        return false;
    }

    private void getLogin(String mobile_no, String selected_pin){

        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Login> call = apiService.login(mobile_no, selected_pin);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {

                Util.hideProgressDialog(progressDialog);
                if(response!=null && response.isSuccessful()){
                    Login login = response.body();
                    UserDetails details = login.getUserDetails();
                    try{
                        if(login.getCode() == 200){
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_ID, details.getMuiId());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_LOGIN_ID, details.getMuiUserId());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_MOBILE, details.getMuiMobile());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_EMAIL, details.getMuiEmail());
                            //SessionManager.getInstance(LoginActivity.this).setInt(SessionManager.USER_LOGIN, login.getAiAppLogin());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_NAME, details.getMuiFirstName()  + " " +login.getUserDetails().getMuiLastName());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_TYPE, details.getMuiUserType());
                            SessionManager.getInstance(LoginActivity.this).setString(SessionManager.USER_CREATE_BY_ID, details.getMuiCreatedById());
                            //SessionManager.getInstance(LoginActivity.this).setString(SessionManager.RANDOM_ATTENDANCE, details.getAi_random_attendance());
                            Log.e(TAG, "user_id : " + details.getMuiId() + "\nuser type : " + details.getMuiUserType() + "\nuser type : " + details.getMuiCreatedById());
                            //Log.e(TAG, " code : " + login.getCode() +" message : " + login.getMessage() + "  id : " + details.getMuiUserType());
                            startActivity(new Intent(LoginActivity.this, HomePage.class));
                            finish();

                        }
                        else{
                            ActionForAll.myFlash(getApplicationContext(), login.getMessage());
                            pinViewLogin.setText("");
                            Log.e(TAG, "message : " + login.getMessage() + "  code : " + login.getCode() );
                        }

                    }catch (Exception e){
                        Log.e(TAG, "exception  " + e.toString());
                    }
                }
                Log.e(TAG, "onResponse : " + response.toString());
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }
}