
package in.etsoft.addpaymini.model.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("mui_id")
    @Expose
    private String muiId;
    @SerializedName("mui_user_type")
    @Expose
    private String muiUserType;
    @SerializedName("mui_company_type")
    @Expose
    private String muiCompanyType;
    @SerializedName("mui_created_by_type")
    @Expose
    private String muiCreatedByType;
    @SerializedName("mui_created_by_id")
    @Expose
    private String muiCreatedById;
    @SerializedName("mui_company_name")
    @Expose
    private String muiCompanyName;
    @SerializedName("mui_first_name")
    @Expose
    private String muiFirstName;
    @SerializedName("mui_last_name")
    @Expose
    private String muiLastName;
    @SerializedName("mui_mobile")
    @Expose
    private String muiMobile;
    @SerializedName("mui_email")
    @Expose
    private String muiEmail;
    @SerializedName("mui_address")
    @Expose
    private String muiAddress;
    @SerializedName("mui_state_id")
    @Expose
    private String muiStateId;
    @SerializedName("mui_city_id")
    @Expose
    private String muiCityId;
    @SerializedName("mui_pin_code")
    @Expose
    private String muiPinCode;
    @SerializedName("mui_account_holder_name")
    @Expose
    private String muiAccountHolderName;
    @SerializedName("mui_bank_name")
    @Expose
    private String muiBankName;
    @SerializedName("mui_account_number")
    @Expose
    private String muiAccountNumber;
    @SerializedName("mui_ifsc_code")
    @Expose
    private String muiIfscCode;
    @SerializedName("mui_pan_number")
    @Expose
    private String muiPanNumber;
    @SerializedName("mui_pan_docs")
    @Expose
    private String muiPanDocs;
    @SerializedName("mui_aadhar_number")
    @Expose
    private String muiAadharNumber;
    @SerializedName("mui_aadhar_front_docs")
    @Expose
    private String muiAadharFrontDocs;
    @SerializedName("mui_aadhar_back_docs")
    @Expose
    private String muiAadharBackDocs;
    @SerializedName("mui_date")
    @Expose
    private String muiDate;
    @SerializedName("mui_gst_registration")
    @Expose
    private String muiGstRegistration;
    @SerializedName("mui_gst_number")
    @Expose
    private String muiGstNumber;
    @SerializedName("mui_status")
    @Expose
    private String muiStatus;
    @SerializedName("mui_user_id")
    @Expose
    private String muiUserId;
    @SerializedName("mui_mef1")
    @Expose
    private String muiMef1;
    @SerializedName("mui_mef2")
    @Expose
    private String muiMef2;
    @SerializedName("mui_user_pin")
    @Expose
    private String muiUserPin;

    @SerializedName("mui_updated_date")
    @Expose
    private String muiUpdatedDate;

    public String getMui_passport_photo() {
        return mui_passport_photo;
    }

    public void setMui_passport_photo(String mui_passport_photo) {
        this.mui_passport_photo = mui_passport_photo;
    }

    public String getMui_payment_recive_docs() {
        return mui_payment_recive_docs;
    }

    public void setMui_payment_recive_docs(String mui_payment_recive_docs) {
        this.mui_payment_recive_docs = mui_payment_recive_docs;
    }

    @SerializedName("mui_passport_photo")
    @Expose
    private String mui_passport_photo;

    @SerializedName("mui_payment_recive_docs")
    @Expose
    private String mui_payment_recive_docs;

    public String getMui_cancelled_check() {
        return mui_cancelled_check;
    }

    public void setMui_cancelled_check(String mui_cancelled_check) {
        this.mui_cancelled_check = mui_cancelled_check;
    }

    @SerializedName("mui_cancelled_check")
    @Expose
    private String mui_cancelled_check;

    public String getMuiId() {
        return muiId;
    }

    public void setMuiId(String muiId) {
        this.muiId = muiId;
    }

    public String getMuiUserType() {
        return muiUserType;
    }

    public void setMuiUserType(String muiUserType) {
        this.muiUserType = muiUserType;
    }

    public String getMuiCompanyType() {
        return muiCompanyType;
    }

    public void setMuiCompanyType(String muiCompanyType) {
        this.muiCompanyType = muiCompanyType;
    }

    public String getMuiCreatedByType() {
        return muiCreatedByType;
    }

    public void setMuiCreatedByType(String muiCreatedByType) {
        this.muiCreatedByType = muiCreatedByType;
    }

    public String getMuiCreatedById() {
        return muiCreatedById;
    }

    public void setMuiCreatedById(String muiCreatedById) {
        this.muiCreatedById = muiCreatedById;
    }

    public String getMuiCompanyName() {
        return muiCompanyName;
    }

    public void setMuiCompanyName(String muiCompanyName) {
        this.muiCompanyName = muiCompanyName;
    }

    public String getMuiFirstName() {
        return muiFirstName;
    }

    public void setMuiFirstName(String muiFirstName) {
        this.muiFirstName = muiFirstName;
    }

    public String getMuiLastName() {
        return muiLastName;
    }

    public void setMuiLastName(String muiLastName) {
        this.muiLastName = muiLastName;
    }

    public String getMuiMobile() {
        return muiMobile;
    }

    public void setMuiMobile(String muiMobile) {
        this.muiMobile = muiMobile;
    }

    public String getMuiEmail() {
        return muiEmail;
    }

    public void setMuiEmail(String muiEmail) {
        this.muiEmail = muiEmail;
    }

    public String getMuiAddress() {
        return muiAddress;
    }

    public void setMuiAddress(String muiAddress) {
        this.muiAddress = muiAddress;
    }

    public String getMuiStateId() {
        return muiStateId;
    }

    public void setMuiStateId(String muiStateId) {
        this.muiStateId = muiStateId;
    }

    public String getMuiCityId() {
        return muiCityId;
    }

    public void setMuiCityId(String muiCityId) {
        this.muiCityId = muiCityId;
    }

    public String getMuiPinCode() {
        return muiPinCode;
    }

    public void setMuiPinCode(String muiPinCode) {
        this.muiPinCode = muiPinCode;
    }

    public String getMuiAccountHolderName() {
        return muiAccountHolderName;
    }

    public void setMuiAccountHolderName(String muiAccountHolderName) {
        this.muiAccountHolderName = muiAccountHolderName;
    }

    public String getMuiBankName() {
        return muiBankName;
    }

    public void setMuiBankName(String muiBankName) {
        this.muiBankName = muiBankName;
    }

    public String getMuiAccountNumber() {
        return muiAccountNumber;
    }

    public void setMuiAccountNumber(String muiAccountNumber) {
        this.muiAccountNumber = muiAccountNumber;
    }

    public String getMuiIfscCode() {
        return muiIfscCode;
    }

    public void setMuiIfscCode(String muiIfscCode) {
        this.muiIfscCode = muiIfscCode;
    }

    public String getMuiPanNumber() {
        return muiPanNumber;
    }

    public void setMuiPanNumber(String muiPanNumber) {
        this.muiPanNumber = muiPanNumber;
    }

    public String getMuiPanDocs() {
        return muiPanDocs;
    }

    public void setMuiPanDocs(String muiPanDocs) {
        this.muiPanDocs = muiPanDocs;
    }

    public String getMuiAadharNumber() {
        return muiAadharNumber;
    }

    public void setMuiAadharNumber(String muiAadharNumber) {
        this.muiAadharNumber = muiAadharNumber;
    }

    public String getMuiAadharFrontDocs() {
        return muiAadharFrontDocs;
    }

    public void setMuiAadharFrontDocs(String muiAadharFrontDocs) {
        this.muiAadharFrontDocs = muiAadharFrontDocs;
    }

    public String getMuiAadharBackDocs() {
        return muiAadharBackDocs;
    }

    public void setMuiAadharBackDocs(String muiAadharBackDocs) {
        this.muiAadharBackDocs = muiAadharBackDocs;
    }

    public String getMuiDate() {
        return muiDate;
    }

    public void setMuiDate(String muiDate) {
        this.muiDate = muiDate;
    }

    public String getMuiGstRegistration() {
        return muiGstRegistration;
    }

    public void setMuiGstRegistration(String muiGstRegistration) {
        this.muiGstRegistration = muiGstRegistration;
    }

    public String getMuiGstNumber() {
        return muiGstNumber;
    }

    public void setMuiGstNumber(String muiGstNumber) {
        this.muiGstNumber = muiGstNumber;
    }

    public String getMuiStatus() {
        return muiStatus;
    }

    public void setMuiStatus(String muiStatus) {
        this.muiStatus = muiStatus;
    }

    public String getMuiUserId() {
        return muiUserId;
    }

    public void setMuiUserId(String muiUserId) {
        this.muiUserId = muiUserId;
    }

    public String getMuiMef1() {
        return muiMef1;
    }

    public void setMuiMef1(String muiMef1) {
        this.muiMef1 = muiMef1;
    }

    public String getMuiMef2() {
        return muiMef2;
    }

    public void setMuiMef2(String muiMef2) {
        this.muiMef2 = muiMef2;
    }

    public String getMuiUserPin() {
        return muiUserPin;
    }

    public void setMuiUserPin(String muiUserPin) {
        this.muiUserPin = muiUserPin;
    }

    public String getMuiUpdatedDate() {
        return muiUpdatedDate;
    }

    public void setMuiUpdatedDate(String muiUpdatedDate) {
        this.muiUpdatedDate = muiUpdatedDate;
    }

}
