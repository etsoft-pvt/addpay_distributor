
package in.etsoft.addpaymini.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_details")
    @Expose
    private UserDetails userDetails;
    @SerializedName("app_login_status")
    @Expose
    private Integer appLoginStatus;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public Integer getAppLoginStatus() {
        return appLoginStatus;
    }

    public void setAppLoginStatus(Integer appLoginStatus) {
        this.appLoginStatus = appLoginStatus;
    }

}
