
package in.etsoft.addpaymini.model.formlist;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Forms {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("leave_data")
    @Expose
    private List<LeaveDatum> leaveData = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<LeaveDatum> getLeaveData() {
        return leaveData;
    }

    public void setLeaveData(List<LeaveDatum> leaveData) {
        this.leaveData = leaveData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
