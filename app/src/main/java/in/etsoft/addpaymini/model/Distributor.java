
package in.etsoft.addpaymini.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Distributor {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("distributor_count")
    @Expose
    private Integer distributorCount;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getDistributorCount() {
        return distributorCount;
    }

    public void setDistributorCount(Integer distributorCount) {
        this.distributorCount = distributorCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
