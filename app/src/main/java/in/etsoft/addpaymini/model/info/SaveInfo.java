
package in.etsoft.addpaymini.model.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveInfo {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("personal_info_update_id")
    @Expose
    private Integer personalInfoUpdateId;
    @SerializedName("user_data")
    @Expose
    private UserData userData;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getPersonalInfoUpdateId() {
        return personalInfoUpdateId;
    }

    public void setPersonalInfoUpdateId(Integer personalInfoUpdateId) {
        this.personalInfoUpdateId = personalInfoUpdateId;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
