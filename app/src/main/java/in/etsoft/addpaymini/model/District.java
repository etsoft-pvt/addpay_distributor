
package in.etsoft.addpaymini.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class District {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("district_list")
    @Expose
    private List<DistrictList> districtList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<DistrictList> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<DistrictList> districtList) {
        this.districtList = districtList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
