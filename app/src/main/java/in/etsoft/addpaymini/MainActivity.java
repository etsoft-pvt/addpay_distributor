package in.etsoft.addpaymini;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.Slide;
import androidx.transition.TransitionManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import id.zelory.compressor.Compressor;
import in.etsoft.addpaymini.adapters.DistrictAdapter;
import in.etsoft.addpaymini.adapters.StateAdapter;
import in.etsoft.addpaymini.model.District;
import in.etsoft.addpaymini.model.DistrictList;
import in.etsoft.addpaymini.model.State;
import in.etsoft.addpaymini.model.StateList;
import in.etsoft.addpaymini.model.formlist.Forms;
import in.etsoft.addpaymini.model.info.SaveInfo;
import in.etsoft.addpaymini.model.info.UserData;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.network_lib.UrlConstants;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.CameraUtils;
import in.etsoft.addpaymini.utility.Formstate;
import in.etsoft.addpaymini.utility.SessionManager;
import in.etsoft.addpaymini.utility.Util;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    Context context = MainActivity.this;
    public static final String GALLERY_DIRECTORY_NAME = "Distributor";
    public static final String IMAGE_EXTENSION = "jpg";
    private String[] user_type_super = {"Master Distributor", "Distributor", "Retailer"};
    private String[] user_type_master = {"Distributor", "Retailer"};
    private String[] user_type_distributor = {"Retailer"};
    private String[] company_type = {"Individual", "One Person Company", "Proprietorship", "Partnership", "LLP", "PVT LTD"};
    String company_name = "";
    String val_company_type = "";
    String et_text_gst = "";
    String state_id = "";
    String district_id = "";
    String user_type = "0";
    String user_type_name = "";
    boolean isGST = false;
    boolean isIndividual = false;
    private Spinner spinUserType, spinState;
    private Spinner spinComType, spinCity;
    private TextView tvYes, tvNo, tvCompanyNameTag, tvGSTNumberTag;
    private ImageView ivRequestImage, ivPassportPic;
    private EditText etGSTNum, etCompanyName, etFirstName, etLastName, etMobileNumber, etEmail, etAddress, etPin;
    private Button btn_ma_next, btn_ma_save;
    ArrayAdapter<String> adapterUserType;
    ArrayAdapter<String> adapterCompanyType;
    String gstRegistration = "";
    String save_com_name = "";
    String save_com_type = "";
    String save_fname = "";
    String save_lname = "";
    String save_mobile = "";
    String save_email = "";
    String save_address = "";
    String save_gst_num = "";
    String save_gst_registration = "";
    String save_pin_code = "";
    String save_user_type = "";
    String save_user_type_name = "";
    String save_state_id = "";
    String save_district_id = "";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    // key to store image path in savedInstance state
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";
    public static final int MEDIA_TYPE_IMAGE = 1;
    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;
    private static String imageStoragePath = "";
    ImageView iv_camera_pan;
    public File compressToFileProfile = null;
    Formstate formstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        setTitle("User Personal Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadView();
        restoreFromBundle(savedInstanceState);
    }

    private void loadView() {
        formstate = Formstate.getInstance(context);
        if (SessionManager.getInstance(context).getBoolean(SessionManager.ADD_USER)) {
            formstate.clearSession();
        }
        spinUserType = (Spinner) findViewById(R.id.SpinUserType);
        spinComType = (Spinner) findViewById(R.id.spinComType);
        spinState = (Spinner) findViewById(R.id.spinState);
        spinCity = (Spinner) findViewById(R.id.spinCity);
        tvYes = (TextView) findViewById(R.id.tvYes);
        tvNo = (TextView) findViewById(R.id.tvNo);
        tvCompanyNameTag = (TextView) findViewById(R.id.tvCompanyNameTag);
        tvGSTNumberTag = (TextView) findViewById(R.id.tvGSTNumberTag);
        etGSTNum = (EditText) findViewById(R.id.etGSTNum);
        etCompanyName = (EditText) findViewById(R.id.etCompanyName);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNumber);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etPin = (EditText) findViewById(R.id.etPin);
        etEmail = (EditText) findViewById(R.id.etEmail);
        btn_ma_next = (Button) findViewById(R.id.btn_ma_next);
        btn_ma_save = (Button) findViewById(R.id.btn_ma_save);
        ivRequestImage = (ImageView) findViewById(R.id.ivRequestImage);
        ivPassportPic = (ImageView) findViewById(R.id.ivPassportPic);
        etGSTNum.setText(Formstate.getInstance(context).getString(Formstate.GST_NUMBER));
        tvYes.setOnClickListener(this);
        tvNo.setOnClickListener(this);
        btn_ma_next.setOnClickListener(this);
        btn_ma_save.setOnClickListener(this);
        ivRequestImage.setOnClickListener(this);
        disableNextButton();
        getState();
        if (SessionManager.getInstance(context).getString(SessionManager.USER_TYPE).equalsIgnoreCase("1")) {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_master);
        } else if (SessionManager.getInstance(context).getString(SessionManager.USER_TYPE).equalsIgnoreCase("2")) {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_distributor);
        } else {
            adapterUserType = new ArrayAdapter<>(this, R.layout.spin_text, user_type_super);
        }
        adapterUserType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinUserType.setAdapter(adapterUserType);
        adapterCompanyType = new ArrayAdapter<>(this, R.layout.spin_text, company_type);
        adapterCompanyType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinComType.setAdapter(adapterCompanyType);
        setValues();
        // set selection
        spinUserType.setSelection(getIndex(spinUserType, save_user_type_name));
        spinComType.setSelection(getIndex(spinComType, save_com_type));
        spinUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (parent.getItemAtPosition(position).toString()) {
                    case "Master Distributor":
                        user_type = "1";
                        user_type_name = "Master Distributor";
                        break;
                    case "Distributor":
                        user_type = "2";
                        user_type_name = "Distributor";
                        break;
                    case "Retailer":
                        user_type = "3";
                        user_type_name = "Retailer";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinComType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    isIndividual = true;
                    val_company_type = parent.getItemAtPosition(position).toString();
                    etCompanyName.setVisibility(View.GONE);
                    tvCompanyNameTag.setVisibility(View.GONE);
                } else {
                    isIndividual = false;
                    val_company_type = parent.getItemAtPosition(position).toString();
                    etCompanyName.setVisibility(View.VISIBLE);
                    tvCompanyNameTag.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void saveValues() {
        if (ActionForAll.validEditText(etFirstName, "first name", MainActivity.this) &&
                ActionForAll.validEditText(etLastName, "last name", MainActivity.this) &&
                ActionForAll.validMobileEditText(etMobileNumber, "mobile number", MainActivity.this) &&
                ActionForAll.isValidEmail(etEmail, MainActivity.this) &&
                ActionForAll.validEditText(etAddress, "address", MainActivity.this) &&
                ActionForAll.validEditText(etPin, "pin code", MainActivity.this)) {
            if (isGST) {
                if (ActionForAll.validEditText(etGSTNum, "GST number", MainActivity.this)) {
                    gstRegistration = "1";
                    et_text_gst = etGSTNum.getText().toString();
                    //startActivity(new Intent(context, BankDetail.class));
                }
            } else {
                gstRegistration = "0";
                et_text_gst = "";
            }
            if (!isIndividual) {
                if (ActionForAll.validEditText(etCompanyName, "company name", MainActivity.this)) {
                    company_name = etCompanyName.getText().toString();
                }
            } else {
                company_name = "";
            }

            if (Formstate.getInstance(context).getString(Formstate.PASS_PORT_PIC).length() > 0) {
                ActionForAll.glideClient(context, UrlConstants.IMAGE_URL +
                        "upload/mini_appuser_image/user_photo/" +
                        Formstate.getInstance(context).getString(Formstate.PASS_PORT_PIC), ivPassportPic);
            }
            /*Log.e(TAG, "---------------------------------------------");
            Log.e(TAG, "company : " + company_name);
            Log.e(TAG, "val_company_type : " + val_company_type);
            Log.e(TAG, "f name : " + etFirstName.getText().toString());
            Log.e(TAG, "l name : " + etLastName.getText().toString());
            Log.e(TAG, "mobile : " + etMobileNumber.getText().toString());
            Log.e(TAG, "email : " + etEmail.getText().toString());
            Log.e(TAG, "address : " + etAddress.getText().toString());
            Log.e(TAG, "gst no : " + et_text_gst);
            Log.e(TAG, "gst registration : " + gstRegistration);
            Log.e(TAG, "pin code : " + etPin.getText().toString());
            Log.e(TAG, "user_type : " + user_type);
            Log.e(TAG, "user_type_name : " + user_type_name);
            Log.e(TAG, "state_id : " + state_id);
            Log.e(TAG, "district_id : " + district_id);
            Log.e(TAG, "image  : " + UrlConstants.BASE_URL +
                    "upload/mini_appuser_image/user_photo/" +
                    Formstate.getInstance(context).getString(Formstate.PASS_PORT_PIC));*/
            uploadFiles();
        }
    }

    private void clickYes() {
        tvYes.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvNo.setTextColor(getResources().getColor(R.color.colorGray));
        etGSTNum.setVisibility(View.VISIBLE);
        tvGSTNumberTag.setVisibility(View.VISIBLE);
        isGST = true;
    }

    private void clickNo() {
        tvNo.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvYes.setTextColor(getResources().getColor(R.color.colorGray));
        etGSTNum.setVisibility(View.GONE);
        tvGSTNumberTag.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvYes:
                //Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
                clickYes();
                break;
            case R.id.tvNo:
                //Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();
                clickNo();
                break;
            case R.id.btn_ma_next:
                startActivity(new Intent(context, BankDetail.class));
                break;
            case R.id.btn_ma_save:
                saveValues();
                break;
            case R.id.ivRequestImage:
                alertPictureChoose();
                break;
        }
    }

    private void setValues() {
        save_user_type = Formstate.getInstance(context).getString(Formstate.USER_TYPE);
        save_user_type_name = Formstate.getInstance(context).getString(Formstate.USER_TYPE_NAME);
        save_com_type = Formstate.getInstance(context).getString(Formstate.COM_TYPE);
        save_com_name = Formstate.getInstance(context).getString(Formstate.COM_NAME);
        //save_com_name
        etCompanyName.setText(save_com_name);
        if (save_com_name.length() > 0)
            tvCompanyNameTag.setVisibility(View.VISIBLE);
        save_gst_registration = Formstate.getInstance(context).getString(Formstate.GST_REGISTRATION);
        if (save_gst_registration.equalsIgnoreCase("1")) {
            clickYes();
        } else {
            clickNo();
        }
        save_gst_num = Formstate.getInstance(context).getString(Formstate.GST_NUMBER);
        etGSTNum.setText(save_gst_num);
        save_fname = Formstate.getInstance(context).getString(Formstate.F_NAME);
        etFirstName.setText(save_fname);
        save_lname = Formstate.getInstance(context).getString(Formstate.L_NAME);
        etLastName.setText(save_lname);
        save_mobile = Formstate.getInstance(context).getString(Formstate.MOBILE);
        etMobileNumber.setText(save_mobile);
        save_email = Formstate.getInstance(context).getString(Formstate.EMAIL);
        etEmail.setText(save_email);
        save_address = Formstate.getInstance(context).getString(Formstate.ADDRESS);
        etAddress.setText(save_address);
        save_state_id = Formstate.getInstance(context).getString(Formstate.STATE_ID);
        save_district_id = Formstate.getInstance(context).getString(Formstate.DISTRICT_ID);
        save_pin_code = Formstate.getInstance(context).getString(Formstate.PIN_CODE);
        etPin.setText(save_pin_code);
        if (save_mobile.length() > 0 && save_email.length() > 0) {
            enableNextButton();
        } else {
            disableNextButton();
        }

        if (formstate.getString(Formstate.PASS_PORT_PIC).length() > 0) {
            ActionForAll.glideClient(context, UrlConstants.IMAGE_URL +
                    "upload/mini_appuser_image/user_photo/" +
                    Formstate.getInstance(context).getString(Formstate.PASS_PORT_PIC), ivPassportPic);

            //https://www.addpay.in/addpaymini/upload/mini_appuser_image/user_photo/1615980562_1601834674_1f0e7.jpg
        }
    }

    private void getState() {
        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<State> call = apiService.getState();
        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                Util.hideProgressDialog(progressDialog);
                if (response != null && response.isSuccessful()) {
                    State state = response.body();
                    try {
                        if (state.getCode() == 200) {
                            List<StateList> stateList = state.getStateList();
                            StateAdapter arrayAdapterDept = new StateAdapter(MainActivity.this, stateList);
                            spinState.setAdapter(arrayAdapterDept);
                            spinState.setSelection(getIndexState(spinState, save_state_id, stateList));
                            spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    //ActionForAll.myFlash(context, stateList.get(position).getId());
                                    state_id = stateList.get(position).getId();
                                    getDistrict(state_id);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                            //Log.e(TAG, " code : " + login.getCode() +" message : " + login.getMessage() + "  date : " + login.getUserDetails().getAiDate() );
                        } else {
                            //Log.e(TAG, "message : " + state.getMessage() + "  code : " + state.getCode());
                        }
                    } catch (Exception e) {
                        // Log.e(TAG, "exception  " + e.toString());
                    }
                }
                //Log.e(TAG, "onResponse : " + response.toString());
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                //Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }

    private void getDistrict(String state_id) {
        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<District> call = apiService.getDistrict(state_id);
        call.enqueue(new Callback<District>() {
            @Override
            public void onResponse(Call<District> call, Response<District> response) {
                Util.hideProgressDialog(progressDialog);
                if (response != null && response.isSuccessful()) {
                    District district = response.body();
                    try {
                        if (district.getCode() == 200) {
                            List<DistrictList> districtList = district.getDistrictList();
                            DistrictAdapter arrayAdapterDept = new DistrictAdapter(MainActivity.this, districtList);
                            spinCity.setAdapter(arrayAdapterDept);
                            spinCity.setSelection(getIndexCity(spinCity, save_district_id, districtList));
                            spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    district_id = districtList.get(position).getId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                            //Log.e(TAG, " code : " + login.getCode() +" message : " + login.getMessage() + "  date : " + login.getUserDetails().getAiDate() );
                        } else {
                            //Log.e(TAG, "message : " + district.getMessage() + "  code : " + district.getCode());
                        }
                        Log.i(TAG, "code / onResponse  : " + district.getCode() + " : " + district.getMessage());

                    } catch (Exception e) {
                        //Log.e(TAG, "exception  " + e.toString());
                    }
                }
                Log.i(TAG, "onResponse : " + response.toString());
            }

            @Override
            public void onFailure(Call<District> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                //Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }

    private void disableNextButton() {
        btn_ma_next.setClickable(false);
        btn_ma_next.setEnabled(false);
        btn_ma_next.setBackgroundResource(R.drawable.button_desable);
    }

    private void enableNextButton() {
        btn_ma_next.setClickable(true);
        btn_ma_next.setEnabled(true);
        btn_ma_next.setBackgroundResource(R.drawable.button_red);
    }

    private String notNullValue(String val) {
        if (!val.isEmpty())
            return val;
        return "";
    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;
        if (myString != null && myString.length() > 0) {
            for (int i = 0; i < spinner.getCount(); i++) {
                if (spinner.getItemAtPosition(i).equals(myString)) {
                    index = i;
                }
            }
        }
        return index;
    }

    private int getIndexState(Spinner spinner, String state_code, List<StateList> list) {
        int index = 0;
        if (state_code != null && state_code.length() > 0) {
            for (StateList state : list) {
                if (state.getId().equals(state_code)) {
                    index = list.indexOf(state);
                }
            }
        }
        return index;
    }

    private int getIndexCity(Spinner spinner, String city_code, List<DistrictList> list) {
        int index = 0;
        if (city_code != null && city_code.length() > 0) {
            for (DistrictList district : list) {
                if (district.getId().equals(city_code)) {
                    index = list.indexOf(district);
                }
            }
        }
        return index;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK) {
            new AlertDialog.Builder(context)
                    .setIcon(R.drawable.logo)
                    .setTitle(R.string.app_name)
                    .setMessage("Do you want to exit ? All information will be reset!")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }
    // image upload work

    /**
     * Restoring store image path from saved instance state
     */
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Log.e(TAG, "path restoreFromBundle imageStoragePath: " + imageStoragePath);

            if (savedInstanceState.containsKey(KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    }
                }
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void requestCameraPermissionGallery(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                openGallery();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "path onRestoreInstanceState : " + imageStoragePath);
        // get the file url
        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }

    /**
     * Display image from gallery
     */
    private void previewCapturedImage() {
        try {
            Log.e(TAG, "preview : " + imageStoragePath);
            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            ExifInterface ei = null;
            try {
                if (imageStoragePath != null) {
                    ei = new ExifInterface(imageStoragePath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Bitmap rotatedBitmap = null;
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;
                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }
                    ivPassportPic.setImageBitmap(rotatedBitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
                iv_camera_pan.setImageBitmap(bitmap);
            }
            Log.e("path", imageStoragePath);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(context);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
                // successfully captured the image
                // display it in image view
                Log.e(TAG, "path onActivityResult : " + imageStoragePath);
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            }
        }

        if (requestCode == GALLERY_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    imageStoragePath = getPath(context, imageUri);
                    previewCapturedImage();
                    Log.e(TAG, imageStoragePath);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(context, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_IMAGE_REQUEST_CODE);
    }

    private void uploadFiles() {
        File panFile = new File(imageStoragePath);
        RequestBody responseBodyProfile = null;
        //Log.e(TAG, "size : " + actual_file.length());
        MultipartBody.Part body_profile = null;
        if (imageStoragePath.length() > 0) {
            compressToFileProfile = Compressor.getDefault(context).compressToFile(panFile);
            responseBodyProfile = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFileProfile);
            body_profile = MultipartBody.Part.createFormData("upload_passport_photo", compressToFileProfile.getName(), responseBodyProfile);
        }
        //Log.e(TAG, "compressed size : " + compressToFile.length());
        RequestBody usertype = RequestBody.create(MultipartBody.FORM, user_type);
        RequestBody val_companytype = RequestBody.create(MultipartBody.FORM, val_company_type);
        RequestBody val_company_name = RequestBody.create(MultipartBody.FORM, company_name);
        RequestBody val_gstRegistration = RequestBody.create(MultipartBody.FORM, gstRegistration);
        RequestBody val_et_text_gst = RequestBody.create(MultipartBody.FORM, et_text_gst);
        RequestBody val_first_name = RequestBody.create(MultipartBody.FORM, etFirstName.getText().toString());
        RequestBody val_last_name = RequestBody.create(MultipartBody.FORM, etLastName.getText().toString());
        RequestBody val_mobile_no = RequestBody.create(MultipartBody.FORM, etMobileNumber.getText().toString());
        RequestBody val_email = RequestBody.create(MultipartBody.FORM, etEmail.getText().toString());
        RequestBody val_address = RequestBody.create(MultipartBody.FORM, etAddress.getText().toString());
        RequestBody val_state_id = RequestBody.create(MultipartBody.FORM, state_id);
        RequestBody val_district_id = RequestBody.create(MultipartBody.FORM, district_id);
        RequestBody val_pin = RequestBody.create(MultipartBody.FORM, etPin.getText().toString());
        RequestBody val_create_by_user_id = RequestBody.create(MultipartBody.FORM, SessionManager.getInstance(context).getString(SessionManager.USER_ID));
        RequestBody val_login_user_type = RequestBody.create(MultipartBody.FORM, SessionManager.getInstance(context).getString(SessionManager.USER_TYPE));
        RequestBody val_user_create_by_id = RequestBody.create(MultipartBody.FORM, SessionManager.getInstance(context).getString(SessionManager.USER_CREATE_BY_ID));
        RequestBody personal_info_update_id = RequestBody.create(MultipartBody.FORM, getVal(formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID)));
        //Log.e(TAG, "personal info id ::: " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));
        final ProgressDialog progressDialog = Util.showProgressDialog(context);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SaveInfo> call = apiService.savePersonalInfo(
                usertype, val_companytype, val_company_name, val_gstRegistration, val_et_text_gst,
                val_first_name, val_last_name, val_mobile_no, val_email, val_address, val_state_id,
                val_district_id, val_pin, val_create_by_user_id, val_login_user_type, val_user_create_by_id,
                personal_info_update_id, body_profile);
        call.enqueue(new Callback<SaveInfo>() {
            @Override
            public void onResponse(Call<SaveInfo> call, Response<SaveInfo> response) {
                Util.hideProgressDialog(progressDialog);
                if (response != null && response.isSuccessful()) {
                    SaveInfo saveInfo = response.body();
                    try {
                        if (saveInfo.getCode() == 200) {
                            UserData userData = saveInfo.getUserData();
                            formstate.setString(Formstate.COM_NAME, notNullValue(userData.getMuiCompanyName()));
                            formstate.setString(Formstate.COM_TYPE, notNullValue(userData.getMuiCompanyType()));
                            formstate.setString(Formstate.F_NAME, notNullValue(userData.getMuiFirstName()));
                            formstate.setString(Formstate.L_NAME, notNullValue(userData.getMuiLastName()));
                            formstate.setString(Formstate.MOBILE, notNullValue(userData.getMuiMobile()));
                            formstate.setString(Formstate.EMAIL, notNullValue(userData.getMuiEmail()));
                            formstate.setString(Formstate.ADDRESS, notNullValue(userData.getMuiAddress()));
                            formstate.setString(Formstate.GST_NUMBER, notNullValue(userData.getMuiGstNumber()));
                            formstate.setString(Formstate.GST_REGISTRATION, notNullValue(userData.getMuiGstRegistration()));
                            formstate.setString(Formstate.PIN_CODE, notNullValue(userData.getMuiPinCode()));
                            formstate.setString(Formstate.USER_TYPE, notNullValue(userData.getMuiUserType()));
                            formstate.setString(Formstate.USER_TYPE_NAME, user_type_name);
                            formstate.setString(Formstate.STATE_ID, notNullValue(userData.getMuiStateId()));
                            formstate.setString(Formstate.DISTRICT_ID, notNullValue(userData.getMuiCityId()));
                            formstate.setInt(Formstate.PERSONAL_INFO_UPDATE_ID, saveInfo.getPersonalInfoUpdateId());
                            Log.e(TAG, "update_id : " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));
                            Log.e(TAG, "mui_passport_photo : " + formstate.getString(Formstate.PASS_PORT_PIC));
                            enableNextButton();
                            ActionForAll.myFlash(context, saveInfo.getMessage());
                            //Log.e(TAG, " code : " + login.getCode() +" message : " + login.getMessage() + "  date : " + login.getUserDetails().getAiDate() );
                        } else {
                            ActionForAll.alertUserWithCloseActivity(saveInfo.getMessage(), "OK", MainActivity.this);
                            //Log.e(TAG, "message : " + district.getMessage() + "  code : " + district.getCode());
                            Log.e(TAG, "update_id : " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));
                        }

                    } catch (Exception e) {
                        //Log.e(TAG, "exception  " + e.toString());
                    }
                    //Log.e(TAG, "onResponse : code / Message " + saveInfo.getCode() + "/" + saveInfo.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SaveInfo> call, Throwable t) {
                //Log.e(TAG, "on onFailure : " + t.getMessage());
                Util.hideProgressDialog(progressDialog);
            }
        });
    }

    // for gallery
    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private void alertPictureChoose() {
        new android.app.AlertDialog.Builder(this).setIcon(R.drawable.logo)
                .setTitle(R.string.app_name).setMessage("Please choose image via")
                .setPositiveButton("Capture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (CameraUtils.checkPermissions(getApplicationContext())) {
                            captureImage();
                        } else {
                            requestCameraPermission(MEDIA_TYPE_IMAGE);
                        }
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        requestCameraPermissionGallery(MEDIA_TYPE_IMAGE);

                    }
                }).create().show();
    }

    private String getVal(int a) {
        if (a > 0) {
            return a + "";
        }
        return "";
    }
}