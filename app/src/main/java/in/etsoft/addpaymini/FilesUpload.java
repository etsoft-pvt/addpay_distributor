package in.etsoft.addpaymini;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLClassLoader;
import java.util.List;

import id.zelory.compressor.Compressor;
import in.etsoft.addpaymini.model.info.SaveInfo;
import in.etsoft.addpaymini.model.info.UserData;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.network_lib.UrlConstants;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.CameraUtils;
import in.etsoft.addpaymini.utility.Formstate;
import in.etsoft.addpaymini.utility.Util;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilesUpload extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = FilesUpload.class.getSimpleName();
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    // key to store image path in savedInstance state
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";
    public static final int MEDIA_TYPE_IMAGE = 1;
    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;
    public static final String GALLERY_DIRECTORY_NAME = "AddPay Gallery";
    public static final String IMAGE_EXTENSION = "jpg";
    private String txt_pan_no = "";
    private String txt_aadhar_no = "";
    private static String imageStoragePath;
    private static String imageStoragePathPAN = "";
    private static String imageStoragePathAadharFront = "";
    private static String imageStorageAadharBack = "";
    private static String imageStorageMEF1 = "";
    private static String imageStorageMEF2 = "";
    int clickBtn = -1;
    Button btnCaptureImagePAN, btnCaptureAdhanFront, btnCaptureAAdharBack, btnCaptureMEF1, btnCaptureMEF2, btn_fu_submit, btn_fu_back;
    ImageView iv_camera_pan, iv_camera_Aadhar_front, iv_camera_aadhar_back, iv_camera_mef1, iv_camera_mef2;
    Context context = FilesUpload.this;
    EditText etPanNum, etAadharNumber;
    TextView tvLocation, tvTime;
    public File compressToFilePAN = null;
    public File compressToFileAA_FRNT = null;
    public File compressToFileAA_BACK = null;
    public File compressToFile_MEF1 = null;
    public File compressToFile_MEF2 = null;
    Formstate formstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_files_upload);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        loadView();
        restoreFromBundle(savedInstanceState);
    }

    private void loadView() {
        setTitle("Upload Docs Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        formstate = Formstate.getInstance(context);
        btnCaptureImagePAN = (Button) findViewById(R.id.btnCaptureImagePAN);
        btnCaptureAdhanFront = (Button) findViewById(R.id.btnCaptureAdhanFront);
        btnCaptureAAdharBack = (Button) findViewById(R.id.btnCaptureAAdharBack);
        btnCaptureMEF1 = (Button) findViewById(R.id.btnCaptureMEF1);
        btnCaptureMEF2 = (Button) findViewById(R.id.btnCaptureMEF2);
        btn_fu_submit = (Button) findViewById(R.id.btn_fu_submit);
        btn_fu_back = (Button) findViewById(R.id.btn_fu_back);
        iv_camera_pan = (ImageView) findViewById(R.id.iv_camera_pan);
        iv_camera_Aadhar_front = (ImageView) findViewById(R.id.iv_camera_Aadhar_front);
        iv_camera_aadhar_back = (ImageView) findViewById(R.id.iv_camera_aadhar_back);
        iv_camera_mef1 = (ImageView) findViewById(R.id.iv_camera_mef1);
        iv_camera_mef2 = (ImageView) findViewById(R.id.iv_camera_mef2);
        etPanNum = (EditText) findViewById(R.id.etPanNum);
        etAadharNumber = (EditText) findViewById(R.id.etAadharNumber);
        btnCaptureImagePAN.setOnClickListener(this);
        btnCaptureAdhanFront.setOnClickListener(this);
        btnCaptureAAdharBack.setOnClickListener(this);
        btnCaptureMEF1.setOnClickListener(this);
        btnCaptureMEF2.setOnClickListener(this);
        btn_fu_submit.setOnClickListener(this);
        btn_fu_back.setOnClickListener(this);
        setValues();
    }

    /**
     * Restoring store image path from saved instance state
     */
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Log.e(TAG, "path restoreFromBundle imageStoragePath: " + imageStoragePath);
            /*Log.e(TAG, "path restoreFromBundle imageStoragePathPAN: " + imageStoragePathPAN);
            Log.e(TAG, "path restoreFromBundle imageStoragePathAadharFront: " + imageStoragePathAadharFront);
            Log.e(TAG, "path restoreFromBundle imageStorageAadharBack : " + imageStorageAadharBack);
            Log.e(TAG, "path restoreFromBundle imageStorageMEF1 : " + imageStorageMEF1);
            Log.e(TAG, "path restoreFromBundle imageStorageMEF2 : " + imageStorageMEF2);*/

            if (savedInstanceState.containsKey(KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    }
                }
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void requestCameraPermissionGallery(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                openGallery();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "path onRestoreInstanceState : " + imageStoragePath);
        // get the file url
        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }

    /**
     * Display image from gallery
     */
    private void previewCapturedImage() {
        try {
            Log.e(TAG, "preview : " + imageStoragePath);
            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            ExifInterface ei = null;
            try {
                if (imageStoragePath != null) {

                    ei = new ExifInterface(imageStoragePath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                    Bitmap rotatedBitmap = null;
                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }
                    if (clickBtn == 1) {
                        imageStoragePathPAN = imageStoragePath;
                        iv_camera_pan.setImageBitmap(rotatedBitmap);
                    }

                    if (clickBtn == 2) {
                        imageStoragePathAadharFront = imageStoragePath;
                        iv_camera_Aadhar_front.setImageBitmap(rotatedBitmap);
                    }

                    if (clickBtn == 3) {
                        imageStorageAadharBack = imageStoragePath;
                        iv_camera_aadhar_back.setImageBitmap(rotatedBitmap);
                    }

                    if (clickBtn == 4) {
                        imageStorageMEF1 = imageStoragePath;
                        iv_camera_mef1.setImageBitmap(rotatedBitmap);
                    }

                    if (clickBtn == 5) {
                        imageStorageMEF2 = imageStoragePath;
                        iv_camera_mef2.setImageBitmap(rotatedBitmap);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
                iv_camera_pan.setImageBitmap(bitmap);
            }
            Log.e("path", imageStoragePath);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(FilesUpload.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
                // successfully captured the image
                // display it in image view
                Log.e(TAG, "path onActivityResult : " + imageStoragePath);
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            }
        }

        if (requestCode == GALLERY_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    imageStoragePath = getPath(FilesUpload.this, imageUri);
                    previewCapturedImage();
                    Log.e(TAG, imageStoragePath);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(FilesUpload.this, "Something went wrong", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(FilesUpload.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCaptureImagePAN:
                clickBtn = 1;
                alertPictureChoose();
                break;
            case R.id.btnCaptureAdhanFront:
                clickBtn = 2;
                alertPictureChoose();
                break;
            case R.id.btnCaptureAAdharBack:
                clickBtn = 3;
                alertPictureChoose();
                break;
            case R.id.btnCaptureMEF1:
                clickBtn = 4;
                alertPictureChoose();
                break;
            case R.id.btnCaptureMEF2:
                clickBtn = 5;
                alertPictureChoose();
                break;

            case R.id.btn_fu_submit:
                if (etPanNum.getText().toString().trim().length() > 0) {
                    txt_pan_no = etPanNum.getText().toString();
                } else {
                    txt_pan_no = "";
                }

                if (etAadharNumber.getText().toString().trim().length() > 0) {
                    txt_aadhar_no = etAadharNumber.getText().toString();
                } else {
                    txt_aadhar_no = "";
                }
                txt_aadhar_no = etAadharNumber.getText().toString();
                uploadFiles();
                Log.e(TAG, "pan no : " + etPanNum.getText().toString());
                Log.e(TAG, "aadhar no : " + etAadharNumber.getText().toString());
                Log.e(TAG, "path pan : " + imageStoragePathPAN);
                Log.e(TAG, "path aa_f : " + imageStoragePathAadharFront);
                Log.e(TAG, "pan aa_b : " + imageStorageAadharBack);
                Log.e(TAG, "pan mef1 : " + imageStorageMEF1);
                Log.e(TAG, "pan mef2 : " + imageStorageMEF2);
                break;

            case R.id.btn_fu_back:
                onBackPressed();
                break;
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_IMAGE_REQUEST_CODE);
    }

    private void uploadFiles() {

        File panFile = new File(imageStoragePathPAN);
        File aa_F_File = new File(imageStoragePathAadharFront);
        File aa_B_File = new File(imageStorageAadharBack);
        File mef1_File = new File(imageStorageMEF1);
        File mef2_File = new File(imageStorageMEF2);

        RequestBody responseBodyPan = null;
        RequestBody responseBodyAA_F = null;
        RequestBody responseBodyAA_B = null;
        RequestBody responseBodyMEF1 = null;
        RequestBody responseBodyMEF2 = null;
        //Log.e(TAG, "size : " + actual_file.length());
        MultipartBody.Part body_pan = null;
        MultipartBody.Part body_aa_f = null;
        MultipartBody.Part body_aa_b = null;
        MultipartBody.Part body_mef1 = null;
        MultipartBody.Part body_mef2 = null;

        if (imageStoragePathPAN.length() > 0) {
            compressToFilePAN = Compressor.getDefault(FilesUpload.this).compressToFile(panFile);
            responseBodyPan = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFilePAN);
            body_pan = MultipartBody.Part.createFormData("upload_pan", compressToFilePAN.getName(), responseBodyPan);
        }
       /* else{
            responseBodyPan = RequestBody.create(MediaType.parse("multipart/form-data"), panFile);
            body_pan = MultipartBody.Part.createFormData("upload_pan", panFile.getName(),responseBodyPan);
        }*/

        if (imageStoragePathAadharFront.length() > 0) {
            compressToFileAA_FRNT = Compressor.getDefault(FilesUpload.this).compressToFile(aa_F_File);
            responseBodyAA_F = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFileAA_FRNT);
            body_aa_f = MultipartBody.Part.createFormData("upload_aadhar_front", compressToFileAA_FRNT.getName(), responseBodyAA_F);
        }

        if (imageStorageAadharBack.length() > 0) {
            compressToFileAA_BACK = Compressor.getDefault(FilesUpload.this).compressToFile(aa_B_File);
            responseBodyAA_B = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFileAA_BACK);
            body_aa_b = MultipartBody.Part.createFormData("upload_aadhar_back", compressToFileAA_BACK.getName(), responseBodyAA_B);
        }
            /*else{
                responseBodyAA_B = RequestBody.create(MediaType.parse("multipart/form-data"), aa_B_File);
                body_aa_b = MultipartBody.Part.createFormData("upload_aadhar_back", aa_B_File.getName(),responseBodyAA_B);
            }*/

        if (imageStorageMEF1.length() > 0) {
            compressToFile_MEF1 = Compressor.getDefault(FilesUpload.this).compressToFile(mef1_File);
            responseBodyMEF1 = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFile_MEF1);
            body_mef1 = MultipartBody.Part.createFormData("mef1", compressToFile_MEF1.getName(), responseBodyMEF1);

        }

        if (imageStorageMEF2.length() > 0) {
            compressToFile_MEF2 = Compressor.getDefault(FilesUpload.this).compressToFile(mef2_File);
            responseBodyMEF2 = RequestBody.create(MediaType.parse("multipart/form-data"), compressToFile_MEF2);
            body_mef2 = MultipartBody.Part.createFormData("mef2", compressToFile_MEF2.getName(), responseBodyMEF2);
        }
        //Log.e(TAG, "compressed size : " + compressToFile.length());
        RequestBody pan_number = RequestBody.create(MultipartBody.FORM, txt_pan_no);
        RequestBody aadhar_number = RequestBody.create(MultipartBody.FORM, txt_aadhar_no);
        RequestBody personal_info_update_id = RequestBody.create(MultipartBody.FORM, String.valueOf(formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID)));
        Log.e(TAG, "personal info id ::: " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));

        final ProgressDialog progressDialog = Util.showProgressDialog(FilesUpload.this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //Log.e(TAG, ""+pan_number)
        Call<SaveInfo> call = apiService.filesUpload(pan_number, aadhar_number, body_pan, body_aa_f, body_aa_b, body_mef1, body_mef2, personal_info_update_id);
        call.enqueue(new Callback<SaveInfo>() {
            @Override
            public void onResponse(Call<SaveInfo> call, Response<SaveInfo> response) {

                Util.hideProgressDialog(progressDialog);

                if (response != null && response.isSuccessful()) {

                    try {
                        SaveInfo info = response.body();
                        if (info.getCode() == 200) {
                            //Log.e(TAG, attendance.getImageLink());
                            Log.e(TAG, "update_id : " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));
                            Log.e(TAG, "pan num : " + info.getUserData().getMuiPanNumber());
                            Log.e(TAG, "aadhar number : " + info.getUserData().getMuiAadharNumber());
                            Log.e(TAG, "pan  : " + info.getUserData().getMuiPanDocs());
                            Log.e(TAG, "aadhar_back  : " + info.getUserData().getMuiAadharBackDocs());
                            Log.e(TAG, "aadhar_frnt  : " + info.getUserData().getMuiAadharFrontDocs());
                            Log.e(TAG, "mef1  : " + info.getUserData().getMuiMef1());
                            Log.e(TAG, "mef2  : " + info.getUserData().getMuiMef2());
                            UserData data = info.getUserData();
                            if (data.getMuiPanNumber() != null && !data.getMuiPanNumber().isEmpty())
                                formstate.setString(Formstate.PAN_NUMBER, info.getUserData().getMuiPanNumber());
                            if (data.getMuiAadharNumber() != null && !data.getMuiAadharNumber().isEmpty())
                                formstate.setString(Formstate.AADHAR_NUMBER, info.getUserData().getMuiAadharNumber());
                            if (data.getMuiPanDocs() != null && !data.getMuiPanDocs().isEmpty())
                                formstate.setString(Formstate.UPLOAD_PAN, info.getUserData().getMuiPanDocs());
                            if (data.getMuiAadharBackDocs() != null && !data.getMuiAadharBackDocs().isEmpty())
                                formstate.setString(Formstate.UPLOAD_AADHAR_BACK, info.getUserData().getMuiAadharBackDocs());
                            if (data.getMuiAadharFrontDocs() != null && !data.getMuiAadharFrontDocs().isEmpty())
                                formstate.setString(Formstate.UPLOAD_AADHAR_FRONT, info.getUserData().getMuiAadharFrontDocs());
                            if (data.getMuiMef1() != null && !data.getMuiMef1().isEmpty())
                                formstate.setString(Formstate.UPLOAD_MEF1, info.getUserData().getMuiMef1());
                            if (data.getMuiMef2() != null && !data.getMuiMef2().isEmpty())
                                formstate.setString(Formstate.UPLOAD_MEF2, info.getUserData().getMuiMef2());
                            imageStoragePath = "";
                            imageStoragePathPAN = "";
                            imageStoragePathAadharFront = "";
                            imageStorageAadharBack = "";
                            imageStorageMEF1 = "";
                            imageStorageMEF2 = "";
                            ActionForAll.alertUserWithCloseActivity(info.getMessage(), "OK", FilesUpload.this);
                        }
                        //jsonObject.getString("message");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SaveInfo> call, Throwable t) {
                //Log.e(TAG, "on onFailure : " + t.getMessage());
                Util.hideProgressDialog(progressDialog);
            }
        });
    }

    // for gallery
    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private void alertPictureChoose() {
        new AlertDialog.Builder(this).setIcon(R.drawable.logo)
                .setTitle(R.string.app_name).setMessage("Please choose image via")
                .setPositiveButton("Capture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (CameraUtils.checkPermissions(getApplicationContext())) {
                            captureImage();
                        } else {
                            requestCameraPermission(MEDIA_TYPE_IMAGE);
                        }
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        requestCameraPermissionGallery(MEDIA_TYPE_IMAGE);

                    }
                }).create().show();
    }

    private void setValues() {
        String pan_number = formstate.getString(Formstate.PAN_NUMBER);
        String aadhar_number = formstate.getString(Formstate.AADHAR_NUMBER);
        String upload_pan = formstate.getString(Formstate.UPLOAD_PAN);
        String upload_aa_f = formstate.getString(Formstate.UPLOAD_AADHAR_FRONT);
        String upload_aa_back = formstate.getString(Formstate.UPLOAD_AADHAR_BACK);
        String upload_mef1 = formstate.getString(Formstate.UPLOAD_MEF1);
        String upload_mef2 = formstate.getString(Formstate.UPLOAD_MEF2);

        Log.e(TAG, "pan_number : " + pan_number);
        Log.e(TAG, "aadhar_number : " + aadhar_number);
        Log.e(TAG, "upload_pan : " + upload_pan);
        Log.e(TAG, "upload_aa_f : " + upload_aa_f);
        Log.e(TAG, "upload_aa_back : " + upload_aa_back);
        Log.e(TAG, "upload_mef1 : " + upload_mef1);
        Log.e(TAG, "upload_mef2 : " + upload_mef2);

        if (!pan_number.isEmpty() && pan_number.length() > 0)
            etPanNum.setText(pan_number);
        if (!aadhar_number.isEmpty() && aadhar_number.length() > 0)
            etAadharNumber.setText(aadhar_number);

        if (!upload_pan.isEmpty()) {
            ActionForAll.glideClient(context, UrlConstants.CONSTANT_IMAGE_URL_PAN + upload_pan, iv_camera_pan);
            //imageStoragePathPAN = UrlConstants.CONSTANT_IMAGE_URL_PAN + upload_pan;
            Log.e(TAG, "pan upload : " + imageStoragePathPAN);
        }
        if (!upload_aa_f.isEmpty()) {
            ActionForAll.glideClient(context, UrlConstants.CONSTANT_IMAGE_URL_AADHAR + upload_aa_f, iv_camera_Aadhar_front);
            //imageStoragePathAadharFront = UrlConstants.CONSTANT_IMAGE_URL_AADHAR + upload_aa_f;
            //Log.e(TAG, "aa_f upload : " + imageStoragePathAadharFront);
        }
        if (!upload_aa_back.isEmpty()) {
            ActionForAll.glideClient(context, UrlConstants.CONSTANT_IMAGE_URL_AADHAR + upload_aa_back, iv_camera_aadhar_back);
            //imageStorageAadharBack = UrlConstants.CONSTANT_IMAGE_URL_AADHAR + upload_aa_back;
            Log.e(TAG, "aa_b upload : " + imageStorageAadharBack);
        }
        if (!upload_mef1.isEmpty()) {
            ActionForAll.glideClient(context, UrlConstants.CONSTANT_IMAGE_URL_MEF + upload_mef1, iv_camera_mef1);
            //imageStorageMEF1 = UrlConstants.CONSTANT_IMAGE_URL_MEF + upload_mef1;
            //Log.e(TAG, "mef1 upload : " + imageStorageMEF1 );
        }
        if (!upload_mef2.isEmpty()) {
            ActionForAll.glideClient(context, UrlConstants.CONSTANT_IMAGE_URL_MEF + upload_mef2, iv_camera_mef2);
            //imageStorageMEF2 = UrlConstants.CONSTANT_IMAGE_URL_MEF + upload_mef2;
            //Log.e(TAG, "mef1 upload : " + imageStorageMEF2);
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

}