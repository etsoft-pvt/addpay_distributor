package in.etsoft.addpaymini;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.FormatFlagsConversionMismatchException;
import java.util.List;

import id.zelory.compressor.Compressor;
import in.etsoft.addpaymini.adapters.DistrictAdapter;
import in.etsoft.addpaymini.model.District;
import in.etsoft.addpaymini.model.DistrictList;
import in.etsoft.addpaymini.model.info.SaveInfo;
import in.etsoft.addpaymini.model.info.UserData;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import in.etsoft.addpaymini.network_lib.UrlConstants;
import in.etsoft.addpaymini.utility.ActionForAll;
import in.etsoft.addpaymini.utility.CameraUtils;
import in.etsoft.addpaymini.utility.Formstate;
import in.etsoft.addpaymini.utility.Util;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetail extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = BankDetail.class.getSimpleName();
    Context context = BankDetail.this;

    Button btn_ba_next, btn_ba_back, btnSaveBankDetail, btnCapturePaymentReceipt, btnCapturePassbook;
    EditText etAccountHolder, etBankName, etAccountNo, etIFSCCode;
    Formstate formstate;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    // key to store image path in savedInstance state
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";
    public static final int MEDIA_TYPE_IMAGE = 1;
    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 8;
    public static final String IMAGE_EXTENSION = "jpg";
    private static String imageStoragePath = "";
    private static String imageStoragePaymentReceipt = "";
    private static String imageStoragePassbook = "";
    int clickBtn = -1;
    ImageView iv_payment_receipt, iv_bank_passbook;
    public File compressToPayReceipt = null;
    public File compressToPassbook = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);
        loadView();
        restoreFromBundle(savedInstanceState);
    }

    private void loadView() {
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        setTitle("Bank Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        formstate = Formstate.getInstance(context);
        btn_ba_next = (Button) findViewById(R.id.btn_ba_next);
        btn_ba_back = (Button) findViewById(R.id.btn_ba_back);
        btnSaveBankDetail = (Button) findViewById(R.id.btnSaveBankDetail);
        btnCapturePaymentReceipt = (Button) findViewById(R.id.btnCapturePaymentReceipt);
        btnCapturePassbook = (Button) findViewById(R.id.btnCapturePassbook);
        etAccountHolder = (EditText) findViewById(R.id.etAccountHolder);
        etBankName = (EditText) findViewById(R.id.etBankName);
        etAccountNo = (EditText) findViewById(R.id.etAccountNo);
        etIFSCCode = (EditText) findViewById(R.id.etIFSCCode);
        iv_payment_receipt = (ImageView) findViewById(R.id.iv_payment_receipt);
        iv_bank_passbook = (ImageView) findViewById(R.id.iv_bank_passbook);
        btn_ba_next.setOnClickListener(this);
        btn_ba_back.setOnClickListener(this);
        btnSaveBankDetail.setOnClickListener(this);
        btnCapturePaymentReceipt.setOnClickListener(this);
        btnSaveBankDetail.setOnClickListener(this);
        btnCapturePassbook.setOnClickListener(this);
        int personal_info_update_id = Formstate.getInstance(context).getInt(Formstate.PERSONAL_INFO_UPDATE_ID);
        if (personal_info_update_id > 0) {
            enableNextButton();
        } else {
            disableNextButton();
        }
        setValues();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ba_next:
                startActivity(new Intent(context, FilesUpload.class));
                break;
            case R.id.btn_ba_back:
                onBackPressed();
                break;
            case R.id.btnCapturePaymentReceipt:
                clickBtn = 1;
                alertPictureChoose();
                break;
            case R.id.btnCapturePassbook:
                clickBtn = 2;
                alertPictureChoose();
                break;
            case R.id.btnSaveBankDetail:
                if (ActionForAll.validEditText(etAccountHolder, "account holder name", BankDetail.this) &&
                        ActionForAll.validEditText(etBankName, "bank name", BankDetail.this) &&
                        ActionForAll.validEditText(etAccountNo, "account number", BankDetail.this) &&
                        ActionForAll.validEditText(etIFSCCode, "IFSC code", BankDetail.this)) {
                    uploadFiles();
                }
                break;
        }
    }

    private void setValues() {
        etAccountHolder.setText(formstate.getString(Formstate.ACCOUNT_HOLDER_NAME));
        etAccountNo.setText(formstate.getString(Formstate.ACCOUNT_NUMBER));
        etBankName.setText(formstate.getString(Formstate.ACCOUNT_BANK_NAME));
        etIFSCCode.setText(formstate.getString(Formstate.IFSC_CODE));

        if (formstate.getString(Formstate.PAYMENT_RECEIPT_PIC).length() > 0) {
            ActionForAll.glideClient(context, UrlConstants.IMAGE_URL +
                    "upload/mini_appuser_image/bank_docs/" +
                    formstate.getString(Formstate.PAYMENT_RECEIPT_PIC), iv_payment_receipt);
            //https://www.addpay.in/addpaymini/upload/mini_appuser_image/bank_docs/1615981325_1602915996_Adil_Adhar_card_(2).jpg
        }

        if (formstate.getString(Formstate.PASSBOOK_PIC).length() > 0) {
            ActionForAll.glideClient(context, UrlConstants.IMAGE_URL +
                    "upload/mini_appuser_image/bank_docs/" +
                    formstate.getString(Formstate.PASSBOOK_PIC), iv_bank_passbook);
            //https://www.addpay.in/addpaymini/upload/mini_appuser_image/bank_docs/1615981325_1602915996_Adil_Adhar_card_(2).jpg
        }
    }

    private void disableNextButton() {
        btn_ba_next.setClickable(false);
        btn_ba_next.setEnabled(false);
        btn_ba_next.setBackgroundResource(R.drawable.button_desable);
    }

    private void enableNextButton() {
        btn_ba_next.setClickable(true);
        btn_ba_next.setEnabled(true);
        btn_ba_next.setBackgroundResource(R.drawable.button_red);
    }

    private String notNullValue(String val) {
        if (val != null && !val.isEmpty())
            return val;
        return "";
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }
    //image work
    /**
     * Restoring store image path from saved instance state
     */
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Log.e(TAG, "path restoreFromBundle imageStoragePath: " + imageStoragePath);
            if (savedInstanceState.containsKey(KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    }
                }
            }
        }
    }
    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                captureImage();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void requestCameraPermissionGallery(final int type) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {

                            if (type == MEDIA_TYPE_IMAGE) {
                                // capture picture
                                openGallery();
                            }

                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }
    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }
    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e(TAG, "path onRestoreInstanceState : " + imageStoragePath);
        // get the file url
        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }
    /**
     * Display image from gallery
     */
    private void previewCapturedImage() {
        try {
            Log.e(TAG, "preview : " + imageStoragePath);
            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            ExifInterface ei = null;
            try {
                if (imageStoragePath != null) {

                    ei = new ExifInterface(imageStoragePath);
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

                    Bitmap rotatedBitmap = null;
                    switch (orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotatedBitmap = rotateImage(bitmap, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotatedBitmap = rotateImage(bitmap, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotatedBitmap = rotateImage(bitmap, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            rotatedBitmap = bitmap;
                    }
                    if (clickBtn == 1) {
                        imageStoragePaymentReceipt = imageStoragePath;
                        iv_payment_receipt.setImageBitmap(rotatedBitmap);
                    }

                    if (clickBtn == 2) {
                        imageStoragePassbook = imageStoragePath;
                        iv_bank_passbook.setImageBitmap(rotatedBitmap);
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
                //iv_camera_pan.setImageBitmap(bitmap);
            }
            Log.e("path", imageStoragePath);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(context);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
                // successfully captured the image
                // display it in image view
                Log.e(TAG, "path onActivityResult : " + imageStoragePath);
                previewCapturedImage();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
                imageStoragePath = "";
            }
        }

        if (requestCode == GALLERY_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    imageStoragePath = getPath(context, imageUri);
                    previewCapturedImage();
                    Log.e(TAG, imageStoragePath);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(context, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_IMAGE_REQUEST_CODE);
    }

    private void uploadFiles() {

        File filePaymentReceipt = new File(imageStoragePaymentReceipt);
        File fileStoragePassbook = new File(imageStoragePassbook);

        RequestBody responseBodyPayReceipt = null;
        RequestBody responseBodyPassbook = null;

        //Log.e(TAG, "size : " + actual_file.length());
        MultipartBody.Part body_pay_receipt = null;
        MultipartBody.Part body_passbook = null;

        if (imageStoragePaymentReceipt.length() > 0) {
            compressToPayReceipt = Compressor.getDefault(context).compressToFile(filePaymentReceipt);
            responseBodyPayReceipt = RequestBody.create(MediaType.parse("multipart/form-data"), compressToPayReceipt);
            body_pay_receipt = MultipartBody.Part.createFormData("upload_pyment_receipt", compressToPayReceipt.getName(), responseBodyPayReceipt);
        }
       /* else{
            responseBodyPan = RequestBody.create(MediaType.parse("multipart/form-data"), panFile);
            body_pan = MultipartBody.Part.createFormData("upload_pan", panFile.getName(),responseBodyPan);
        }*/

        if (imageStoragePassbook.length() > 0) {
            compressToPassbook = Compressor.getDefault(context).compressToFile(fileStoragePassbook);
            responseBodyPassbook = RequestBody.create(MediaType.parse("multipart/form-data"), compressToPassbook);
            body_passbook = MultipartBody.Part.createFormData("upload_cancelled_check", compressToPassbook.getName(), responseBodyPassbook);
        }

        //Log.e(TAG, "compressed size : " + compressToFile.length());
        RequestBody card_holder_name = RequestBody.create(MultipartBody.FORM, etAccountHolder.getText().toString());
        RequestBody bank_name = RequestBody.create(MultipartBody.FORM, etBankName.getText().toString());
        RequestBody account_no = RequestBody.create(MultipartBody.FORM, etAccountNo.getText().toString());
        RequestBody ifsc_code = RequestBody.create(MultipartBody.FORM, etIFSCCode.getText().toString());
        RequestBody personal_info_update_id = RequestBody.create(MultipartBody.FORM, String.valueOf(formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID)));
        Log.e(TAG, "personal info id ::: " + formstate.getInt(Formstate.PERSONAL_INFO_UPDATE_ID));

        final ProgressDialog progressDialog = Util.showProgressDialog(BankDetail.this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //Log.e(TAG, ""+pan_number)
        Call<SaveInfo> call = apiService.saveBankInfo(card_holder_name, bank_name, account_no, ifsc_code, personal_info_update_id,
                body_pay_receipt, body_passbook);
        call.enqueue(new Callback<SaveInfo>() {
            @Override
            public void onResponse(Call<SaveInfo> call, Response<SaveInfo> response) {

                Util.hideProgressDialog(progressDialog);
                if (response != null && response.isSuccessful()) {
                    SaveInfo saveInfo = response.body();
                    try {
                        if (saveInfo.getCode() == 200) {
                            UserData userData = saveInfo.getUserData();
                            formstate.setInt(Formstate.PERSONAL_INFO_UPDATE_ID, saveInfo.getPersonalInfoUpdateId());
                            formstate.setString(Formstate.ACCOUNT_HOLDER_NAME, notNullValue(userData.getMuiAccountHolderName()));
                            formstate.setString(Formstate.ACCOUNT_BANK_NAME, notNullValue(userData.getMuiBankName()));
                            formstate.setString(Formstate.ACCOUNT_NUMBER, notNullValue(userData.getMuiAccountNumber()));
                            formstate.setString(Formstate.IFSC_CODE, notNullValue(userData.getMuiIfscCode()));
                            formstate.setString(Formstate.PAYMENT_RECEIPT_PIC, notNullValue(userData.getMui_payment_recive_docs()));
                            formstate.setString(Formstate.PASSBOOK_PIC, notNullValue(userData.getMui_cancelled_check()));
                            ActionForAll.myFlash(context, saveInfo.getMessage());
                            Log.e(TAG, " payment_rec : " + userData.getMui_payment_recive_docs() + ", cancel_chek : " + userData.getMui_cancelled_check());
                        } else {

                            ActionForAll.alertUserWithCloseActivity(saveInfo.getMessage(), "OK", BankDetail.this);
                            Log.e(TAG, "message : " + saveInfo.getMessage() + "  code : " + saveInfo.getCode());
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "exception  " + e.toString());
                    }

                    Log.e(TAG, "onResponse : code / Message " + saveInfo.getCode() + "/" + saveInfo.getMessage());
                }

            }

            @Override
            public void onFailure(Call<SaveInfo> call, Throwable t) {
                //Log.e(TAG, "on onFailure : " + t.getMessage());
                Util.hideProgressDialog(progressDialog);
            }
        });
    }

    // for gallery
    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    private void alertPictureChoose() {
        new AlertDialog.Builder(this).setIcon(R.drawable.logo)
                .setTitle(R.string.app_name).setMessage("Please choose image via")
                .setPositiveButton("Capture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (CameraUtils.checkPermissions(getApplicationContext())) {
                            captureImage();
                        } else {
                            requestCameraPermission(MEDIA_TYPE_IMAGE);
                        }
                    }
                })
                .setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        requestCameraPermissionGallery(MEDIA_TYPE_IMAGE);

                    }
                }).create().show();
    }

}