
package in.etsoft.addpaymini.network_lib;


import in.etsoft.addpaymini.DownlineRetailer;
import in.etsoft.addpaymini.model.Distributor;
import in.etsoft.addpaymini.model.District;
import in.etsoft.addpaymini.model.MyResponse;
import in.etsoft.addpaymini.model.Retailer;
import in.etsoft.addpaymini.model.State;
import in.etsoft.addpaymini.model.formlist.Forms;
import in.etsoft.addpaymini.model.info.SaveInfo;
import in.etsoft.addpaymini.model.login.Login;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {


    @GET(UrlConstants.STATE)
    Call<State> getState();

    @POST(UrlConstants.DISTRICT)
    @FormUrlEncoded
    Call<District> getDistrict(@Field("state_id") String state_id);

    @Multipart
    @POST(UrlConstants.SAVE_PERSONAL_INFO)
    Call<SaveInfo> savePersonalInfo(@Part("user_type") RequestBody  user_type,
                                    @Part("company_type") RequestBody  company_type,
                                    @Part("company_name") RequestBody  company_name,
                                    @Part("gst_registration") RequestBody  gst_registration,
                                    @Part("gst_number") RequestBody  gst_number,
                                    @Part("first_name") RequestBody  first_name,
                                    @Part("last_name") RequestBody  last_name,
                                    @Part("mobile") RequestBody  mobile,
                                    @Part("email") RequestBody  email,
                                    @Part("address") RequestBody  address,
                                    @Part("state") RequestBody  state,
                                    @Part("city") RequestBody  city,
                                    @Part("pin") RequestBody  pin,
                                    @Part("created_by_user_id") RequestBody  created_by_user_id,
                                    @Part("login_user_type") RequestBody  login_user_type,
                                    @Part("admin_create_id") RequestBody  admin_create_id,
                                    @Part("personal_info_update_id") RequestBody  personal_info_update_id,
                                    @Part MultipartBody.Part image_profile);


    @Multipart
    @POST(UrlConstants.UPLAOD_FILES)
    Call<SaveInfo> filesUpload(

            @Part("pan_number") RequestBody pan_number,
            @Part("aadhar_number") RequestBody aadhar_number,
            @Part MultipartBody.Part imagePAN,
            @Part MultipartBody.Part imageAAfront,
            @Part MultipartBody.Part imageAAback,
            @Part MultipartBody.Part imageMEF1,
            @Part MultipartBody.Part imageMEF2,
            @Part("personal_info_update_id") RequestBody personal_info_update_id);

    @Multipart
    @POST(UrlConstants.SAVE_BANK_INFO)
    Call<SaveInfo> saveBankInfo(@Part("account_holder") RequestBody account_holder,
                                @Part("bank_name") RequestBody bank_name,
                                @Part("account_number") RequestBody account_number,
                                @Part("ifsc_code") RequestBody ifsc_code,
                                @Part("personal_info_update_id") RequestBody personal_info_update_id,
                                @Part MultipartBody.Part upload_pyment_receipt ,
                                @Part MultipartBody.Part upload_cancelled_check );

    // Customer Registration Process****
    /* @POST(UrlConstants.LOGIN)
    @FormUrlEncoded
    Call<Login> login(@Field("mobile") String mobile_no,
                      @Field("pin") String selected_pin);*/

    @POST(UrlConstants.LOGIN)
    @FormUrlEncoded
    Call<Login> login(
            @Field("mobile") String mobile_no,
            @Field("pin") String selected_pin);

    @POST(UrlConstants.FORM_LIST)
    @FormUrlEncoded
    Call<Forms> formList(@Field("user_login_id") String user_login_id,
                         @Field("user_type") String user_type);

    @POST(UrlConstants.CHANGE_PASSWORD)
    @FormUrlEncoded
    Call<MyResponse> changePassword(@Field("user_login_id") String user_login_id,
                                   @Field("old_pin") String old_pin,
                                    @Field("new_pin") String new_pin);

    @POST(UrlConstants.LOGOUT)
    @FormUrlEncoded
    Call<MyResponse> logout(@Field("user_login_id") String user_login_id);

    @POST(UrlConstants.TOTAL_DOWNLINE_RETAILER)
    @FormUrlEncoded
    Call<DownlineRetailer> total_downline_retailer(@Field("mui_id") String mui_id);

    @POST(UrlConstants.TOTAL_RETAILER)
    @FormUrlEncoded
    Call<Retailer> total_retailer(@Field("mui_id") String mui_id);

    @POST(UrlConstants.TOTAL_DISTRIBUTOR)
    @FormUrlEncoded
    Call<Distributor> total_distributor(@Field("mui_id") String mui_id);

    /*

    *//*@GET(UrlConstants.PROVIDER_DETAIL)
    Call<Detail> getUpdatedDetail(@QueryMap Map<String, String> params);*//*

    @GET(UrlConstants.DEPARTMENT)
    Call<Department> getDepartmentDetail();*/






}

