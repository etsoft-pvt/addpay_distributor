package in.etsoft.addpaymini.network_lib;

public class UrlConstants {

    //Production server
   /* public static final String BASE_URL = "https://www.addpay.in/adplogins/Addpaymini/";
    public static final String IMAGE_URL = "https://www.addpay.in/adplogins/";*/
    //Testing server
    public static final String BASE_URL = " https://www.addpay.in/addpaymini/Addpaymini/";
    public static final String IMAGE_URL = "https://www.addpay.in/addpaymini/";

    public static final String CONSTANT_IMAGE_URL_PAN =  IMAGE_URL + "upload/mini_appuser_image/pan/";
    public static final String CONSTANT_IMAGE_URL_AADHAR = IMAGE_URL + "upload/mini_appuser_image/aadhar/";
    public static final String CONSTANT_IMAGE_URL_MEF = IMAGE_URL + "upload/mini_appuser_image/MEF/";

    public static final String STATE = "get_state_api";
    public static final String DISTRICT = "get_district_api";
    public static final String SAVE_PERSONAL_INFO = "save_user_personal_info_api";
    public static final String SAVE_BANK_INFO = "save_user_bank_info_api";
    public static final String UPLAOD_FILES = "save_user_docs_info_api";
    public static final String LOGIN = "miniuser_login_api";
    public static final String FORM_LIST = "miniuser_list_api" ;
    public static final String CHANGE_PASSWORD = "change_pin_api";
    public static final String LOGOUT = "logout";
    public static final String TOTAL_DISTRIBUTOR = "miniuser_distributor_count_list_api";
    public static final String TOTAL_RETAILER = "miniuser_retailer_count_list_api";
    public static final String TOTAL_DOWNLINE_RETAILER = "miniuser_down_retailer_count_list_api";
}
