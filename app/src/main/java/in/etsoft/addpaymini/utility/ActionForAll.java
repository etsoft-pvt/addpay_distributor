package in.etsoft.addpaymini.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.etsoft.addpaymini.R;

/**
 * Created by admin on 7/20/2017.
 */

public class ActionForAll {

    static Context context;

    public ActionForAll(Context context) {
        this.context = context;
    }

    //============================================= Flash Massage ==============================================================//

    public static void myFlash(Context context, String name) {
        Toast.makeText(context, name, Toast.LENGTH_LONG).show();
    }

    //============================================= Button Clicking Effect =====================================================//

    public static void buttonClickEffect(View button) {

        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }

    //============================================================================================================================

    //=================================================>> Email Validator <<======================================================//

    public static boolean isValidEmail(EditText e , Activity activity) {
        if(!TextUtils.isEmpty(e.getText().toString()) && android.util.Patterns.EMAIL_ADDRESS.matcher(e.getText().toString()).matches()){
            return true;
        }
        else {
            e.setError("Please type valid email id!");
            requestFocus(e, activity);
            return false;
        }
    }

    //=========================================================================================================================//

    //=========================================>> Alert Dialog with one Action <<==============================================//

    public static void alertUser(String massage, String action, final Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.app_name));
        builder.setMessage(massage);
        //builder.setIcon(R.drawable.ic_launcher_foreground);
        builder.setPositiveButton(action,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void alertUserWithCloseActivity(String massage, String action, final Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.app_name));
        builder.setMessage(massage);
        builder.setPositiveButton(action,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // context.startActivity(new Intent(context , LoginActivity.class));
                context.finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    public static void alertChoiseCloseActivity(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.app_name));
        builder.setMessage("Do you want to exit from this page?");
        builder.setPositiveButton("YES",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((Activity)context).finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    //=======================================================================================================================//

    public static boolean  getConnectivityInfo (Context context) {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm != null) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = true;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = true;
                    }
                }
            }
        } else {
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (activeNetwork != null) {
                    // connected to the internet
                    if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                        result = true;
                    } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

   /* public static boolean isNetworkConnected(Context c){
        if(getConnectivityInfo(c)) {
            return true;
        }
        NoIntenetDialog d = new NoIntenetDialog((Activity)c);
        d.show();
        return false;
    }*/

    public static String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }

    /*
     * set font here
     * */

    public static void setFontText(TextView t, Context c)
    {
        Typeface typeface = Typeface.createFromAsset(c.getAssets(),"fonts/Raleway-Regular.ttf");
        t.setTypeface(typeface);
    }

    public static boolean validEditText(EditText edit , String text, Activity act) {
        if (edit.getText().toString().trim().length() < 1) {
            edit.setError("Please enter " + text + " here!");
            requestFocus(edit, act);
            return false;
        }
        return true;
    }

    public static boolean validPassword(EditText edit , String text, Activity act) {
        if (edit.getText().toString().trim().length() != 4) {
            edit.setError("Please enter 4 digit " + text + " here!");
            requestFocus(edit, act);
            return false;
        }
        return true;
    }

    public static boolean validMobileEditText(EditText edit , String text, Activity act) {
        if (edit.getText().toString().trim().length() != 10) {
            edit.setError("Please enter " + text + " here!");
            requestFocus(edit, act);
            return false;
        }
        return true;
    }

    public static void requestFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    //Change Date format***********************************
    public static String getDate(String dateString, String get_pattern_date_format, String req_pattern_date_format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(get_pattern_date_format);
        Date date = simpleDateFormat.parse(dateString);
        SimpleDateFormat sf = new SimpleDateFormat(req_pattern_date_format);
        return sf.format(date);
    }

    public static void glideClient(Context context, String image_url, ImageView imageView){
        Glide.with(context) //1
                .load(image_url)
                .placeholder(R.drawable.background_transparent)
                .error(R.drawable.download)
                .skipMemoryCache(true) //2
                .diskCacheStrategy(DiskCacheStrategy.NONE) //3
                //.transform(CircleCrop()) //4
                .into(imageView);
    }

}
