package in.etsoft.addpaymini.utility;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.etsoft.addpaymini.ListDetail;
import in.etsoft.addpaymini.R;
import in.etsoft.addpaymini.adapters.FormAdapter;
import in.etsoft.addpaymini.model.formlist.Forms;
import in.etsoft.addpaymini.model.formlist.LeaveDatum;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserActivity extends AppCompatActivity {

    private static final String TAG = SearchUserActivity.class.getSimpleName();
    private EditText etSearchUser;
    private RecyclerView recSearch;
    private TextView tvOnUserFound;
    FormAdapter adapter;
    List<LeaveDatum> userList;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        setTitle("Search User");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadView();
        getAllList("1");
    }

    private void loadView() {
        etSearchUser = (EditText) findViewById(R.id.etSearchUser);
        tvOnUserFound = (TextView) findViewById(R.id.tvOnUserFound);
        recSearch = (RecyclerView) findViewById(R.id.recSearch);
        etSearchUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().length() > 0){
                    recSearch.setVisibility(View.VISIBLE);
                }
                else{
                    recSearch.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void getAllList(String user_type) {
        SessionManager manager = SessionManager.getInstance(this);
        //Log.e(TAG, "reason_for_leave: " + "\nDate From : " + date_from + " \nDate to : " + date_to + "\nReason : " + etLeaveReason.getText().toString());
        final ProgressDialog progressDialog = Util.showProgressDialog(this);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.e(TAG,"user id " + manager.getString(SessionManager.USER_ID) + "/user_type " + user_type);
        Call<Forms> call = apiService.formList(manager.getString(SessionManager.USER_ID), user_type);
        call.enqueue(new Callback<Forms>() {
            @Override
            public void onResponse(Call<Forms> call, Response<Forms> response) {
                Util.hideProgressDialog(progressDialog);
                if(response!=null && response.isSuccessful()){
                    Forms myResponse = response.body();
                    try{
                        if(myResponse.getCode() == 200){
                            Log.e(TAG, " code : " + myResponse.getCode() +" req_id : " + myResponse.getMessage());
                            //ActionForAll.myFlash(getApplicationContext(), myResponse.getMessage());
                            recSearch.setVisibility(View.VISIBLE);
                            userList = myResponse.getLeaveData();
                            linearLayoutManager = new LinearLayoutManager(SearchUserActivity.this);
                            linearLayoutManager.setStackFromEnd(true);
                            //linearLayoutManager.setReverseLayout(true);
                            adapter = new FormAdapter(SearchUserActivity.this, userList);
                            recSearch.setLayoutManager(linearLayoutManager);
                            recSearch.setAdapter(adapter);
                            tvOnUserFound.setVisibility(View.GONE);
                            //
                        }
                        else{
                            //ActionForAll.myFlash(getApplicationContext(), myResponse.getMessage());
                            recSearch.setVisibility(View.GONE);
                            tvOnUserFound.setVisibility(View.VISIBLE);
                            ActionForAll.alertUser(myResponse.getMessage(), "OK", SearchUserActivity.this);
                            Log.e(TAG, "message : " + myResponse.getMessage() + "  code : " + myResponse.getCode() );
                        }
                    }catch (Exception e){
                        Log.e(TAG, "exception  " + e.toString());
                    }
                }
                Log.e(TAG, "onResponse : " + response.toString());
            }

            @Override
            public void onFailure(Call<Forms> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                //Log.e(TAG, "onFailure : "+t.toString());
            }
        });
    }

    private void filter(String text) {
         List<LeaveDatum> filterList = new ArrayList<LeaveDatum>();
        //looping through existing elements
        for (LeaveDatum s : userList) {
            //if the existing elements contains the search input
            if (s.getMuiMobile().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter.filterList(filterList);
        if(filterList.size() > 0)
        {
            recSearch.setVisibility(View.VISIBLE);
            tvOnUserFound.setVisibility(View.GONE);
        }
        else{
            recSearch.setVisibility(View.GONE);
            tvOnUserFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}