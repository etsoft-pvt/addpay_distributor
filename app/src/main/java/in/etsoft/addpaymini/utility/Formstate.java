package in.etsoft.addpaymini.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Formstate {

    public static final String TAG = Formstate.class.getSimpleName();
    public static final String COM_NAME = "company_name";
    public static final String GST_NUMBER = "gst_number";
    public static final String COM_TYPE = "com_type";
    public static final String F_NAME = "f_name";
    public static final String L_NAME = "l_name";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String GST_REGISTRATION = "gst_registration";
    public static final String PIN_CODE = "pin_code";
    public static final String USER_TYPE = "user_type";
    public static final String STATE_ID = "state_id";
    public static final String DISTRICT_ID = "district_id";
    public static final String USER_TYPE_NAME = "user_type_name";
    public static final String PERSONAL_INFO_UPDATE_ID = "personal_info_update";
    public static final String ACCOUNT_HOLDER_NAME = "account_holder_name";
    public static final String ACCOUNT_BANK_NAME = "accont_bank_name";
    public static final String ACCOUNT_NUMBER = "accont_number";
    public static final String IFSC_CODE = "ifsc_code";
    public static final String UPLOAD_PAN = "uplaod_pan";
    public static final String UPLOAD_AADHAR_BACK = "upload_aadhar_back";
    public static final String UPLOAD_AADHAR_FRONT = "upload_aadhar_front";
    public static final String UPLOAD_MEF1 = "upload_mef1";
    public static final String UPLOAD_MEF2 = "upload_mef2";
    public static final String PAN_NUMBER = "pan_number";
    public static final String AADHAR_NUMBER = "aadhar_number";
    public static final String PASS_PORT_PIC = "mui_passport_photo";
    public static final String PAYMENT_RECEIPT_PIC = "payment_receipt_pic";
    public static final String PASSBOOK_PIC = "passbook_pic";

    private static String SHARED_PREFERENCE = "state_form";
    ///public static final String COUNT_COMPARE = "count_compare";
    private static SharedPreferences sharedPref;
    private static Context context;
    private static Formstate sharedPrefClass;

    public Formstate(Context context) {
        sharedPref = context.getSharedPreferences(SHARED_PREFERENCE, Activity.MODE_PRIVATE);
    }

    public static Formstate getInstance(Context context) {
        sharedPref = context.getSharedPreferences(SHARED_PREFERENCE, Activity.MODE_PRIVATE);
        Formstate.context = context;
        if (sharedPrefClass == null) {
            sharedPrefClass = new Formstate(context);
            return sharedPrefClass;
        } else {
            return sharedPrefClass;
        }
    }
    public void setString(String key, String value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = Formstate.context.getSharedPreferences(SHARED_PREFERENCE, Activity.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    public void setInt(String key, int value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = Formstate.context.getSharedPreferences(SHARED_PREFERENCE, Activity.MODE_PRIVATE);
        return sharedPref.getInt(key, 0);
    }


    public void setBoolean(String key, boolean value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = Formstate.context.getSharedPreferences(SHARED_PREFERENCE, Activity.MODE_PRIVATE);
        return sharedPref.getBoolean(key, false);
    }

    public void clearSession() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

}
