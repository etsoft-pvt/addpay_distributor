package in.etsoft.addpaymini.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by admin on 7/20/2017.
 */

public class SessionManager {

    public static final String TAG = SessionManager.class.getSimpleName();

    public static final String USER_ID = "user_id";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_NAME = "user_name";
    public static final String USER_LOGIN = "user_is_login";
    public static final String USER_EMAIL = "user_email_id";
    public static final String USER_CHECK_IN_ID = "user_check_in_id";
    public static final String USER_CHECK_IN = "user_check_in";
    public static final String RANDOM_ATTENDANCE = "random_att";
    public static final String USER_CHECK_OUT = "check_out";
    public static final String ADD_USER = "add_user";
    public static final String USER_TYPE = "user_type";
    public static final String USER_LOGIN_ID = "user_login_id";
    public static final String USER_CREATE_BY_ID = "user_create_by_id";

    private static String SHARED_PREFERENCE_ATTENDANCE = "shared_pre_attendance_app";
    ///public static final String COUNT_COMPARE = "count_compare";
    private static SharedPreferences sharedPref;
    private static Context context;
    private static SessionManager sharedPrefClass;


    public SessionManager(Context context) {
        sharedPref = context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
    }

    public static SessionManager getInstance(Context context) {
        sharedPref = context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
        SessionManager.context = context;
        if (sharedPrefClass == null) {
            sharedPrefClass = new SessionManager(context);
            return sharedPrefClass;
        } else {
            return sharedPrefClass;
        }
    }

    public void setString(String key, String value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getString(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = SessionManager.context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    public void setInt(String key, int value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getInt(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = SessionManager.context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
        return sharedPref.getInt(key, 0);
    }


    public void setBoolean(String key, boolean value) {
        Log.e(TAG, " ==> " + key + " / " + value);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        Log.e(TAG, " ==> " + key);
        sharedPref = SessionManager.context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
        return sharedPref.getBoolean(key, false);
    }

   /* public void setObject(String key, Object obj){
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        editor.putString(key, json);
        editor.apply();
    }*/

   /* public Object getObject(String key, Class<T> )
    {
        Gson gson = new Gson();
         sharedPref = SessionManager.context.getSharedPreferences(SHARED_PREFERENCE_ATTENDANCE, Activity.MODE_PRIVATE);
         sharedPref.getString(key, null);
        String json = sharedPref.getString(key, null);
        return  gson.fromJson(json, T.class);
    }*/

    public void clearSession() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

}


