package in.etsoft.addpaymini.utility;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.navigation.NavigationView;

import in.etsoft.addpaymini.ChangePinActivity;
import in.etsoft.addpaymini.ListDetail;
import in.etsoft.addpaymini.LoginActivity;
import in.etsoft.addpaymini.MainActivity;
import in.etsoft.addpaymini.MyCommissionActivity;
import in.etsoft.addpaymini.R;
import in.etsoft.addpaymini.custom_view.CustomNavigationMenuItem;
import in.etsoft.addpaymini.model.MyResponse;
import in.etsoft.addpaymini.network_lib.ApiClient;
import in.etsoft.addpaymini.network_lib.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CodeMinimization {

    private static final String TAG = CodeMinimization.class.getSimpleName();
    //private static FragmentActivity myContext;
    //static SessionManager sessionManager;

    public static Intent in;
    public static Handler handler;

    public static void largeSwitchCase(Context context, String menuItem, DrawerLayout mDrawerLayout) {

        switch (menuItem) {
            case "Home":
                //Toast.makeText(context, "Home", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                break;

            case "Add User":
                SessionManager.getInstance(context).setBoolean(SessionManager.ADD_USER, true);
                //Toast.makeText(context, "Attendance", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in = new Intent(context, MainActivity.class);
                        context.startActivity(in);
                    }
                }, 300);
                break;

            case "List User":
                SessionManager.getInstance(context).setBoolean(SessionManager.ADD_USER,false);
                // Toast.makeText(context, "Leave", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in = new Intent(context, ListDetail.class);
                        context.startActivity(in);
                    }
                }, 300);
                break;

            case "Search User":
                SessionManager.getInstance(context).setBoolean(SessionManager.ADD_USER,false);
                // Toast.makeText(context, "Leave", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in = new Intent(context, SearchUserActivity.class);
                        context.startActivity(in);
                    }
                }, 300);
                break;

            case "Change Pin":
                //Toast.makeText(context, "Complain/Feedback", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in = new Intent(context, ChangePinActivity.class);
                        context.startActivity(in);
                    }
                }, 300);
                break;

                case "My Commission":
                //Toast.makeText(context, "Complain/Feedback", Toast.LENGTH_LONG).show();
                closeDrawerLayout(mDrawerLayout);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        in = new Intent(context, MyCommissionActivity.class);
                        context.startActivity(in);
                    }
                }, 300);
                break;

            case "Logout":
                new AlertDialog.Builder(context)
                        .setTitle(R.string.app_name)
                        .setIcon(R.drawable.logo)
                        .setMessage("Do you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                closeDrawerLayout(mDrawerLayout);
                                logout_user(SessionManager.getInstance(context).getString(SessionManager.USER_LOGIN_ID), context);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                closeDrawerLayout(mDrawerLayout);
                            }
                        }).create().show();

                break;
        }
    }

    public static void navListener(ImageView nav, final DrawerLayout mDrawerLayout) {

        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    public static void navigationItemListener(final NavigationView mNavigationView, final Context context, final DrawerLayout mDrawerLayout) {
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                CodeMinimization.largeSwitchCase(context, menuItem.getTitle().toString(), mDrawerLayout);
                return true;
            }
        });

        Menu m = mNavigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, context);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi, context);
        }
    }

    public static void applyFontToMenuItem(MenuItem mi, Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomNavigationMenuItem("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private static void closeDrawerLayout(DrawerLayout mDrawerLayout) {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private static void logout_user(String user_login_id, Context context) {
        final ProgressDialog progressDialog = Util.showProgressDialog(context);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MyResponse> call = apiService.logout(user_login_id);
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {

                if (response != null && response.isSuccessful()) {

                    Util.hideProgressDialog(progressDialog);
                    MyResponse response1 = response.body();
                    if (response1.getCode() == 200) {
                        SessionManager.getInstance(context).clearSession();
                        Formstate.getInstance(context).clearSession();
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                    } else {
                        ActionForAll.myFlash(context, response1.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Util.hideProgressDialog(progressDialog);
                ActionForAll.myFlash(context, t.getMessage());
            }
        });
    }

}
